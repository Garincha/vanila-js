resultCrtl = document.getElementById("result");// getting the object( here it is result) & assigning it inside resultCrtl
        //console.log(resultCrtl);
        let SimpleCalculator = {
            "add" : function(number1,number2){
                return  parseInt(number1) + parseInt(number2);
            },
            "subtract" : function(number1,number2){
                return parseInt(number1) - parseInt(number2);
            },
            "multiply" : function(number1,number2){
                return parseInt(number1) * parseInt(number2);
            },
            "division" : function(number1,number2){
                return parseInt(number1) / parseInt(number2);
            },
            "Modulus" : function(number1,number2){
                return parseInt(number1) % parseInt(number2);
            } 
            
        }

        let btnAdd = document.getElementById("addition");
        btnAdd.addEventListener('click',function(){
            let number1 = document.getElementById('number1').value;//collecting/getting the value of number1(object) & assigning it inside let number1.
            let number2 = document.getElementById('number2').value;
            let result = SimpleCalculator.add(number1,number2);//sending the value as an argument by the function SimpleCalculator.
            console.log(result);
            resultCrtl.value = result;// showing the result inside result field
        },false);

        let btnSub = document.getElementById("subtraction");
        btnSub.addEventListener('click',function(){
            let number1 = document.getElementById('number1').value;//collecting/getting the values of number1 & assigning it inside let number1.
            let number2 = document.getElementById('number2').value;
            let result = SimpleCalculator.subtract(number1,number2);//sending the value as an argument by the function SimpleCalculator.
            console.log(result);
            resultCrtl.value = result;// showing the result inside result field
        },false);

        let btnMulti = document.getElementById("multiplication");
        btnMulti.addEventListener('click',function(){
            let number1 = document.getElementById('number1').value;//collecting/getting the values of number1 & assigning it inside let number1.
            let number2 = document.getElementById('number2').value;
            let result = SimpleCalculator.multiply(number1,number2);//sending the value as an argument by the function SimpleCalculator.
            console.log(result);
            resultCrtl.value = result;// showing the result inside result field
        },false);

        let btnDiv = document.getElementById("division");
        btnDiv.addEventListener('click',function(){
            let number1 = document.getElementById('number1').value;//collecting/getting the values of number1 & assigning it inside let number1.
            let number2 = document.getElementById('number2').value;
            let result = SimpleCalculator.division(number1,number2);//sending the value as an argument by the function SimpleCalculator.
            console.log(result);
            resultCrtl.value = result;// showing the result inside result field
        },false);

        let btnMod = document.getElementById("modulus");
        btnMod.addEventListener('click',function(){
            let number1 = document.getElementById('number1').value;//collecting/getting the values of number1 & assigning it inside let number1.
            let number2 = document.getElementById('number2').value;
            let result = SimpleCalculator.Modulus(number1,number2);//sending the value as an argument by the function SimpleCalculator.
            console.log(result);
            resultCrtl.value = result;// showing the result inside result field
        },false);