const database = [
    {
    "serial" : 1,
    "id": "5968dd23fc13ae04d9000001",
    "product_name": "sildenafil citrate",
    "supplier": "Wisozk Inc",
    "quantity": 261,
    "unit_cost": "$10.47"
  }, 
  {
    "serial" : 2,
    "id": "5968dd23fc13ae04d9000002",
    "product_name": "Mountain Juniperus ashei",
    "supplier": "Keebler-Hilpert",
    "quantity": 292,
    "unit_cost": "$8.74"
  }, 
  {
    "serial" : 3,
    "id": "5968dd23fc13ae04d9000003",
    "product_name": "Dextromathorphan HBr",
    "supplier": "Schmitt-Weissnat",
    "quantity": 211,
    "unit_cost": "$20.53"
  }
];

const table = document.getElementById('database_list');

for(const key of database){
    let tr = '';
    tr += `<tr align="center">`;
    tr += `<td>`+key.serial+`</td>`;
    tr += `<td>`+key.id+`</td>`;
    tr += `<td>`+key.product_name+`</td>`;
    tr += '<td>'+key.supplier+'</td>';
    tr += `<td>`+key.quantity+`</td>`;
    tr += `<td>`+key.unit_cost+`</td>`;

    table.innerHTML += tr;
    console.log(tr);
}
  