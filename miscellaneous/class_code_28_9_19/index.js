"use strict";
/*
function getData(){
    const products = [
        {
            "id" : 1,
            "type" : 'Laptop',
            "brand" : 'HP',
            "price" : 45000, 
        },
        {
            "id" : 2,
            "type" : 'Mouse',
            "brand" : 'A4TEC',
            "price" : 300, 
        },
        {
            "id" : 3,
            "type" : 'Monitor',
            "brand" : 'Logitec',
            "price" : 5000, 
        }
    
    ];

    return products;
}

console.log(getData());

const products = getData();
*/


/*
this part is for normal function declaration in JS. 
const getData = function (){// Assigning a function inside a variable (getData) is called function literal. 
    const products = [
        {
            "id" : 1,
            "type" : 'Laptop',
            "brand" : 'HP',
            "price" : 45000, 
        },
        {
            "id" : 2,
            "type" : 'Mouse',
            "brand" : 'A4TEC',
            "price" : 300, 
        },
        {
            "id" : 3,
            "type" : 'Monitor',
            "brand" : 'Logitec',
            "price" : 5000, 
        }
    
    ];

    return products;
};

const products = getData();


const table = document.getElementById('product-list')

for(const product of products){

    let tr = "";

    tr += `<tr>`;
    tr += `<td>`+product.id+`</td>`;
    tr += `<td>`+product.type+`</td>`;
    tr += `<td>`+product.brand+`</td>`;
    tr += `<td>`+product.price+`</td>`;
    tr += '</tr>';

    table.innerHTML += tr;
    console.log(tr);
}

*/

const data = (function (){
    const products = [
        {
            "id" : 1,
            "type" : 'Laptop',
            "brand" : 'HP',
            "price" : 45000, 
        },
        {
            "id" : 2,
            "type" : 'Mouse',
            "brand" : 'A4TEC',
            "price" : 300, 
        },
        {
            "id" : 3,
            "type" : 'Monitor',
            "brand" : 'Logitec',
            "price" : 5000, 
        }
    
    ];

    return products;
})(); // first, we're grouping the function(by (function(){}) this way), later we're invoking the function to execute (by (function(){}() this way). this is called IIFE(Imediately invoke function expression) : it always one time execute;

/*function table(){
        


    return table;
}
*/

//const table = document.getElementById('product-list')

let table = document.createElement('table');//Virtual DOM (creating table element)

for(const product of data){

    let itemIdText = document.createTextNode(product.id);//creating text 
    //console.log(itemIdText);
    let itemIdTd = document.createElement('td');// creating td
    itemIdTd.appendChild(itemIdText);// assigning text inside td as td's child by appendchild().  

    let itemTypeText = document.createTextNode(product.type);
    let itemTypeTd = document.createElement('td');
    itemTypeTd.appendChild(itemTypeText);

    let itemBrandText = document.createTextNode(product.brand);
    let itemBrandTd = document.createElement('td');
    itemBrandTd.appendChild(itemBrandText);

    let itemPriceText = document.createTextNode(product.price);
    let itemPriceTd = document.createElement('td');
    itemPriceTd.appendChild(itemPriceText);

    let tr = document.createElement('tr');//creating tr, to have all td inside this tr. 
    tr.appendChild(itemIdTd);//creating tr for id.
    tr.appendChild(itemTypeTd);//creating tr for type
    tr.appendChild(itemBrandTd);//creating tr for brand
    tr.appendChild(itemPriceTd);//creating tr for price

    table.appendChild(tr);// assigning/appending all tr inside table

    
/*
    tr += `<tr>`;
    tr += `<td>`+product.id+`</td>`;
    tr += `<td>`+product.type+`</td>`;
    tr += `<td>`+product.brand+`</td>`;
    tr += `<td>`+product.price+`</td>`;
    tr += '</tr>';

    table.innerHTML += tr;
    console.log(tr);
*/

}

console.log(table);
let container = document.querySelector("#root");//document.querySelector is an alternative of document.getElementById. 
container.appendChild(table);