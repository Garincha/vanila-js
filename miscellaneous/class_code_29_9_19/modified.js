"use strict";

const data = (function (){
    const products = [
        {
            "id" : 1,
            "type" : 'Laptop',
            "brand" : 'HP',
            "price" : 45000, 
        },
        {
            "id" : 2,
            "type" : 'Mouse',
            "brand" : 'A4TEC',
            "price" : 300, 
        },
        {
            "id" : 3,
            "type" : 'Monitor',
            "brand" : 'Logitec',
            "price" : 5000, 
        }
    
    ];

    return products;
})(); // first, we're grouping the function(by (function(){}) this way), later we're invoking the function to execute (by (function(){}() this way). this is called IIFE(Imediately invoke function expression) : it always one time execute;

/*function table(){
        


    return table;
}
*/

//const table = document.getElementById('product-list')

function createTableHead(headerText){
    let headContent = document.createTextNode(headerText);//creating text inside th. 
    let head = document.createElement('TH');//creating table head
    head.appendChild(headContent);//appending text inside th row

    return head;// returning the th with text. 
}

let table = document.createElement('table');//Virtual DOM (creating table element)

let tr = document.createElement('tr');//creating row
tr.appendChild(createTableHead('ID'));// appending the function to add a th with text(inside th). 
tr.appendChild(createTableHead('Type'));
tr.appendChild(createTableHead('Brand'));
tr.appendChild(createTableHead('Price'));
table.appendChild(tr);//appending all tr inside table. 

/* this code is commented, because we have a function createTableHead(headerText){} above,by that we're getting the same ouput. 
because of that function we don't need to repeat the code like here, we've reduced code, made some reuseable code. 
let thIdText = document.createTextNode('id');//creating text word-> id
let thId = document.createElement('th');//creating table head
thId.appendChild(thIdText);//appending text inside table head
tr.appendChild(thId);//appending table head inside table row

let thTypeText = document.createTextNode('type');//creating text word-> type
let thType = document.createElement('th');//creating table head
thType.appendChild(thTypeText);//appending text inside table head
tr.appendChild(thType);//appending table head inside table row

let thBrandText = document.createTextNode('brand');//creating text word-> brand
let thBrand = document.createElement('th');//creating table head
thBrand.appendChild(thBrandText);//appending text inside table head
tr.appendChild(thBrand);//appending table head inside table row

let thPriceText = document.createTextNode('price');//creating text word-> price
let thPrice = document.createElement('th');//creating table head
thPrice.appendChild(thPriceText);//appending text inside table head
tr.appendChild(thPrice);//appending table head inside table row

table.appendChild(tr);//appending all table row inside table
//above this code(from let table = document.createElement('table') to table.appendChild(tr)), we've created {<tr><th></th></tr>} output through html. 

*/
for(const product of data){

    let itemIdText = document.createTextNode(product.id);//creating text 
    //console.log(itemIdText);
    let itemIdTd = document.createElement('td');// creating td
    itemIdTd.appendChild(itemIdText);// assigning text inside td as td's child by appendchild().  

    let itemTypeText = document.createTextNode(product.type);
    let itemTypeTd = document.createElement('td');
    itemTypeTd.appendChild(itemTypeText);

    let itemBrandText = document.createTextNode(product.brand);
    let itemBrandTd = document.createElement('td');
    itemBrandTd.appendChild(itemBrandText);

    let itemPriceText = document.createTextNode(product.price);
    let itemPriceTd = document.createElement('td');
    itemPriceTd.appendChild(itemPriceText);

    let tr = document.createElement('tr');//creating tr, to have all td inside this tr. 
    tr.appendChild(itemIdTd);//creating tr for id.
    tr.appendChild(itemTypeTd);//creating tr for type
    tr.appendChild(itemBrandTd);//creating tr for brand
    tr.appendChild(itemPriceTd);//creating tr for price

    table.appendChild(tr);// assigning/appending all tr inside table

    
/*
    tr += `<tr>`;
    tr += `<td>`+product.id+`</td>`;
    tr += `<td>`+product.type+`</td>`;
    tr += `<td>`+product.brand+`</td>`;
    tr += `<td>`+product.price+`</td>`;
    tr += '</tr>';

    table.innerHTML += tr;
    console.log(tr);
*/

}

console.log(table);
let container = document.querySelector("#root");//document.querySelector is an alternative of document.getElementById. 
container.appendChild(table);