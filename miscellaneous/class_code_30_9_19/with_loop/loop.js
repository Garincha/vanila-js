"use strict";

const data = (function () {
    const products = [{
            "id": 1,
            "type": 'Laptop',
            "brand": 'HP',
            "price": 45000,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 2,
            "type": 'Mouse',
            "brand": 'A4TEC',
            "price": 300,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 3,
            "type": 'Monitor',
            "brand": 'Logitec',
            "price": 5000,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 4,
            "type": 'External disk',
            "brand": 'Transend',
            "price": 8000,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 5,
            "type": 'Pen Drive',
            "brand": 'Transend',
            "price": 300,
            "xyz": 'abc',
            "xyz2": 'efg',
        }

    ];

    return products;
})(); // first, we're grouping the function(by (function(){}) this way), later we're invoking the function to execute (by (function(){}() this way). this is called IIFE(Imediately invoke function expression) : it always one time execute;


//const table = document.getElementById('product-list')

function createTableHead(headerText) {
    let headContent = document.createTextNode(headerText); //creating text inside th. 
    let head = document.createElement('TH'); //creating table head
    head.appendChild(headContent); //appending text inside th row

    return head; // returning the th with text. 
}

let table = document.createElement('table'); //Virtual DOM (creating table element)

let tr = document.createElement('tr'); //creating row
//console.log(data[0]);// only looking for the first positioned object of data array. 
for (let property in data[0]) { //by this loop, getting all heads together dynamically
    //console.log(property);
    tr.appendChild(createTableHead(property.toUpperCase())); // by property, getting o positioned objects all property(other positioned objects properties are same with o positioned object. here property is id,type,brand,prize,xyz,xyz2)
}

/*
tr.appendChild(createTableHead('ID'));// appending the function to add a th with text(inside th). 
tr.appendChild(createTableHead('Type'));
tr.appendChild(createTableHead('Brand'));
tr.appendChild(createTableHead('Price'));
tr.appendChild(createTableHead('XYZ'));
tr.appendChild(createTableHead('XYZ2'));
table.appendChild(tr);//appending all tr inside table. 
*/

/* this code is commented, because we have a function createTableHead(headerText){} above,by that we're getting the same ouput. 
because of that function we don't need to repeat the code like here, we've reduced code, made some reuseable code. 
let thIdText = document.createTextNode('id');//creating text word-> id
let thId = document.createElement('th');//creating table head
thId.appendChild(thIdText);//appending text inside table head
tr.appendChild(thId);//appending table head inside table row

let thTypeText = document.createTextNode('type');//creating text word-> type
let thType = document.createElement('th');//creating table head
thType.appendChild(thTypeText);//appending text inside table head
tr.appendChild(thType);//appending table head inside table row

let thBrandText = document.createTextNode('brand');//creating text word-> brand
let thBrand = document.createElement('th');//creating table head
thBrand.appendChild(thBrandText);//appending text inside table head
tr.appendChild(thBrand);//appending table head inside table row

let thPriceText = document.createTextNode('price');//creating text word-> price
let thPrice = document.createElement('th');//creating table head
thPrice.appendChild(thPriceText);//appending text inside table head
tr.appendChild(thPrice);//appending table head inside table row
*/
table.appendChild(tr); //appending all table row inside table
//above this code(from let table = document.createElement('table') to table.appendChild(tr)), we've created {<tr><th></th></tr>} output through html. 



function createTableCell(cellText) {

    //create text node
    let textNode = document.createTextNode(cellText);
    //create cell/container
    let container = document.createElement('TD');
    //insert text node within container
    container.appendChild(textNode);

    return container;
}
for (const product of data) {
    let td = "";
    let tr = document.createElement('tr'); //creating tr, to have all td inside tr. 
    for (let property in product) { // this loop is a inner loop. by this loop getting all td with value dynamically

        let td = createTableCell(product[property]); //by product[property], we will getting the value(from json name/value pair data)
        tr.appendChild(td); //appending the td with tr

    }
    console.log(tr);

    /*
        let cellId = createTableCell(product.id);//to get the function's returned value(here it is product.id), assigned it inside let cellId. 
        

        let cellType = createTableCell(product.type);//to get the function's returned value(here it is product.type), assigned it inside let cellType.
        tr.appendChild(cellType);

        let cellBrand = createTableCell(product.brand);//to get the function's returned value(here it is product.id), assigned it inside let cellBrand.
        tr.appendChild(cellBrand);

        let cellPrice = createTableCell(product.price);//to get the function's returned value(here it is product.id), assigned it inside let cellPrice. 
        tr.appendChild(cellPrice);

        let cellXyz = createTableCell(product.xyz);
        tr.appendChild(cellXyz);
     */






    table.appendChild(tr); // assigning/appending all tr inside table

}

console.log(table);
let container = document.querySelector("#root"); //document.querySelector is an alternative of document.getElementById. 
container.appendChild(table);