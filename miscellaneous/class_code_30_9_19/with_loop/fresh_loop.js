"use strict";

const data = (function () {
    const products = [{
            "id": 1,
            "type": 'Laptop',
            "brand": 'HP',
            "price": 45000,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 2,
            "type": 'Mouse',
            "brand": 'A4TEC',
            "price": 300,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 3,
            "type": 'Monitor',
            "brand": 'Logitec',
            "price": 5000,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 4,
            "type": 'External disk',
            "brand": 'Transend',
            "price": 8000,
            "xyz": 'abc',
            "xyz2": 'efg',
        },
        {
            "id": 5,
            "type": 'Pen Drive',
            "brand": 'Transend',
            "price": 300,
            "xyz": 'abc',
            "xyz2": 'efg',
        }

    ];

    return products;
})();

function createTableHead(headerText) {
    let headContent = document.createTextNode(headerText); //creating text inside th. 
    let head = document.createElement('TH'); //creating table head
    head.appendChild(headContent); //appending text inside th row

    return head; // returning the th with text. 
}

function createTableCell(cellText) {

    //create text node
    let textNode = document.createTextNode(cellText);
    //create cell/container
    let container = document.createElement('TD');
    //insert text node within container
    container.appendChild(textNode);

    return container;
}

// preparing the table for the data
// table

let table = document.createElement('table'); // Virtual DOM
//thead
let tr = document.createElement('tr');
for (let property in data[0]) {
    tr.appendChild(createTableHead(property.toUpperCase()));
}
table.appendChild(tr);
// table rows
for (let product of data) {
    let td = '';
    let tr = document.createElement('tr'); // find the reason why

    for (let property in product) {
        td = createTableCell(product[property])
        tr.appendChild(td)

    }
    table.appendChild(tr);
}
let container = document.querySelector("#root")
container.appendChild(table);