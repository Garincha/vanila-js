"use strict"

const output1 = document.getElementById('output1');
const output2 = document.getElementById('output2');

const myObject = {
    "people" : [{
        "firstName" : "Alan",
        "lastName" : "Shepherd",
        "age" : 29,
    },
    {
        "firstName" : `Rud`,
        "lastName" : `Gulit`,
        "age" : 50,
    }
]
};
console.log(myObject);

//const i = 0;

//output1.innerHTML = myObject.people[0].firstName;
output1.innerHTML = myObject.people[0].lastName;
//output1.innerHTML = myObject.people[0].age;

//const j = 1;

output2.innerHTML = myObject.people[1].firstName;
output2.innerHTML = myObject.people[1].lastName;
output2.innerHTML = myObject.people[1].age;