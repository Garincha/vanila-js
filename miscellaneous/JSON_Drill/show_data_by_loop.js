"use script"

const output = document.getElementById('output');
const output2 = document.getElementById('output2');

const myObject = {
    "people" : [
        {
            "firstName" : "James",
            "lastName" : "Gaslong",
            "age" : 45,
        },
        {
            "firstName" : "Robert",
            "lastName" : "Hook",
            "age" : 55,
        }
    ],
    "places" : [
        {
            "location" : "NewYork",
            "lat" : 135,
            "long" : 88,
        },
        {
            "location" : "Toronto",
            "lat" : 234,
            "long" : 58,
        }
    ]
};

for(let i = 0;i < myObject.people.length;i++){
    output.innerHTML += "<br>"+myObject.people[i].firstName + " " + myObject.people[i].lastName+ " "+ myObject.people[i].age;
}

for(let j = 0; j < myObject.places.length; j++){
    output.innerHTML += "<br>" + myObject.places[j].location + " " + myObject.places[j].lat+ " " + myObject.places[j].long;
}