"use strict"

const cars = {
    "car" : ["blue","black"]
};
console.log(cars);

const myJson = {
    "car" : {"color":`grey`},
    "car2" : {"color" : `light blue`}
};
console.log(myJson);

const myJson2 = {
    "car" : {"color" : `black`,"model" : `Vox Wagon`},
    "car2" : {"color" : `silver`,"model" : `Mercidiz`}
};
console.log(myJson2);