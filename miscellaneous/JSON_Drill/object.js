"use strict"
const output1 = document.getElementById('output1');
const output2 = document.getElementById('output2');

const myObject = {
    "firstName":`Samuel`,
    "lastName" : `Morrison`,
    "age" : 35,
};

console.log(myObject);

const name = 'Name';

output1.innerHTML = myObject.firstName;
output2.innerHTML = myObject['last'+name];