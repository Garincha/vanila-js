"use strict"
const myJson = {};
myJson.car1 = `black`;
console.log(myJson);
myJson.car2 = `blue`;
console.log(myJson);

const myJson2 = {};
myJson2["car1"] = `red`;
console.log(myJson2);
myJson2["car2"] = `green`;
console.log(myJson2);

const myJson3 = {"car1" : "yellow", "car2" : "pink"};
console.log(myJson3);