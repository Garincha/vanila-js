"use strict";
const animals = [
    {
            "name": "Cat",
            "ageInYear": 1,
            "numberOfLegs": 4,
            "numberOfTail": 1,
            "numberOfEye": 2,
            "gender": "male"
        },
        {
            "name": "Dog",
            "ageInYear": 5,
            "numberOfLegs": 4,
            "numberOfTail": 1,
            "numberOfEye": 2,
            "gender": "male"
        },
        {
            "name": "Cow",
            "ageInYear": 4,
            "numberOfLegs": 4,
            "numberOfTail": 1,
            "numberOfEye": 2,
            "gender": "female"
        },
        {
            "name": "goat",
            "ageInYear": 3,
            "numberOfLegs": 4,
            "numberOfTail": 1,
            "numberOfEye": 2,
            "gender": "female"
        }
    
];

console.log(animals);