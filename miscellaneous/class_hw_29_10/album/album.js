'use strict';


let data = (function () {
    const albums = [
        {

            "userId": 1,
            "id": 1,
            "title": "quidem molestiae enim"
        
        },
        {
        
            "userId": 1,
            "id": 2,
            "title": "sunt qui excepturi placeat culpa"
        
        },
        {
        
            "userId": 1,
            "id": 3,
            "title": "omnis laborum odio"
        
        },
        {
        
            "userId": 1,
            "id": 4,
            "title": "non esse culpa molestiae omnis sed optio"
        
        },
        {
        
            "userId": 1,
            "id": 5,
            "title": "eaque aut omnis a"
        
        },
    ];

    return albums;

})();

const John = {};

John.htmlAlbum = {
    displayUserId : function(userId){
        let userIdText = document.createTextNode(data.userId);
        let h4 = document.createElement('h4');
        h4.appendChild(userIdText);
        return h4;
    },
    displayId : function (id) {
        let idText = document.createTextNode(data.id);
        let h4 = document.createElement('h4');
        h4.appendChild(idText);
        return h4;
    },
    displayTitle : function (title) {
        let TitleText = document.createTextNode(data.title);
        let p = document.createElement('p');
        p.appendChild(TitleText);
        return p;
    },
    prepareAlbum : function (data) {
        let album = document.createElement('div');
        let albumUserId = '';
        let albumId = '';
        let albumTitle = '';
        
        albumUserId = this.displayUserId(data);
        album.appendChild(albumUserId);
        albumId = this.displayId(data);
        album.appendChild(albumId);
        albumTitle = this.displayTitle(data);
        album.appendChild(albumTitle);
        //post.setAttribute('style','display : inline-block');
        return album;
    },
    display : function (data, domLocaation) {
        let container = document.querySelector(domLocaation);
        let showAlbum = this.prepareAlbum(data);
        container.appendChild(showAlbum);
        
        return true;
    }
};

John.htmlAlbum.display(data,'#root');