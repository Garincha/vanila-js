* Objective : Displaying Multiple comments into a post by comment JSON (API)



0 : Collecting comments data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .

1 : Preparing Commenter Name 

Step 1A : Creating container for commenter name . e.g - using em node

Step 1B : Creating text node.

Step 1C : Inserting text node into container node. e.g - inside em node

2 : Preparing Commenter Email  

Step 2A : Creating container for commenter email . e.g - strong node

Step 2B : Creating text node .

Step 2C : Inserting text into container node.

3 : Preparing Comments Body

Step 3A : Creating container for comment body/detail/description. e.g - span node.

Step 3B : Creating text node for body/description content.

Step 3C : Inserting text into comment detail container . e.g - inside span node.


4 : Preparing Single Comment

Step 4A : Creating comment container. e.g - using p node

Step 4B : Inserting  commenter name (1), comments body (2), commenter email (3) into comment container .

Step 4C : Creating another comment container . e.g - using div node.

Step 4D : Inserting all nodes into container. e.g - inside div node.

5 : Preparing All Comments

Step 5A : Repeat 1,2,3,4 .

5 : Display the Comment

Step 5A : Target the Location

Step 5B : Insert all Comments into targeted HTML tree.