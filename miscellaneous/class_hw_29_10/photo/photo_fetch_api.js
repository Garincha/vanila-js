"use strict";

const John = {};// this is a top level packet/object

John.BlogPhotos = {// this is a property of John 



 getImageSource : function(imageUrl) {
    
    let imageSource = null;

    let imageContainer = document.createElement('img');

    imageContainer.setAttribute('src',imageUrl);

    imageContainer.setAttribute('style','width: 200px; height: 200px');

    imageSource = imageContainer;
    console.log(imageSource);
    return imageSource;

},




 getImageTitleText : function(text){
    
    let imageTitleText = null;
    
    let imageTitleTextContainer = document.createElement('figcaption');

    let imageTitleTextDetails = document.createTextNode(text);

    imageTitleTextContainer.appendChild(imageTitleTextDetails);
    
    imageTitleText = imageTitleTextContainer;
    console.log(imageTitleText);
    return imageTitleText;
},

getImageLinkComment : function (comment) {
    
    let imageLinkComment = null;

    let imageLinkCommentContainer = document.createElement('a');

    imageLinkCommentContainer.setAttribute('href',comment);

    let imageLinkCommentText = document.createTextNode(comment);

    imageLinkCommentContainer.appendChild(imageLinkCommentText);

    imageLinkComment = imageLinkCommentContainer;
    console.log( imageLinkComment);
    return imageLinkComment;
},

getImageBody : function (photoData) {
    
    let imageBody = null;

    let imageSubComponentContainer = document.createElement('figure');

    imageBody = imageSubComponentContainer;

    return imageBody;
},


getImage : function(photoData){
    
    let image = null;

    let imageMainContainer = document.createElement('div');
    console.log(photoData);
    let imageSource = this.getImageSource(photoData.url);
    let imageTitleText = this.getImageTitleText(photoData.title);
    //let imageLinkComment = this.getImageLinkComment(photoData.comment);
    let imageBody = this.getImageBody(photoData.body);
    imageMainContainer.appendChild(imageSource);
    imageMainContainer.appendChild(imageTitleText);
    //imageMainContainer.appendChild(imageLinkComment);
    imageMainContainer.appendChild(imageBody);
 

    image = imageMainContainer; //(container + content)

    return image;

},

display : function(Location,photosData) {

    //Step 5A : Target the Location
    let htmlLocation = document.querySelector(Location);
    let photo = null;
    for(let photoData of photosData){
         photo = this.getImage(photoData);
        htmlLocation.appendChild(photo);
        console.log(photo);
    
    }
    
   console.log(htmlLocation); 
},


};
/*
window.data = null;
 John.RemoteData = {
    data : null,
    retrieve : function(){
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(function(posts){
             //console.log(posts)
            //John.BlogPosts.display('#root', posts);
            //console.log(window);
            window.data = posts;
  });
    }
};


John.RemoteData.retrieve();
console.log(window.data);
//4 : Preparing All Posts 
*/
//Step 4A : Repeat 1,2,3 .




//5 : Display the Post (populate into html tree)





/*
for(let postDataIndex in postsData){
    let post = getPost(postsData[postDataIndex].title,postsData[postDataIndex].body);
    htmlLocation.appendChild(post);
    console.log(postDataIndex);
}
*/


//let postsData = null;
function loadPhotos(){
    fetch('https://jsonplaceholder.typicode.com/photos')
        .then(response => response.json())
        .then(function(photos){
             //console.log(photos);
            John.BlogPhotos.display('#root', photos);
            //console.log(window);
            //window.data = posts;
  });

}
//display('#root',postsData);//calling the function with two arguments. 


//Step 5B : Insert all posts into targeted HTML tree.

/*
let post2 = getPost(postsData[1].title,postsData[1].body);
htmlLocation.appendChild(post2);

let post3 = getPost(postsData[2].title,postsData[2].body);
htmlLocation.appendChild(post3);

let post4 = getPost(postsData[3].title,postsData[3].body);
htmlLocation.appendChild(post4);
*/

