"use strict"
/*
* Objective : Displaying Multiple posts from a photo JSON (API)



0 : Collecting phots data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .
*/

//Step 1 : Prepare photo component . e.g - making a photo.

//Step 1A : Prepare image/pictue sub component. e.g - figure tag.

let imageContainer = document.createElement('img');

imageContainer.setAttribute('src','https://via.placeholder.com/600/92c952');
imageContainer.setAttribute('style','width: 200px; height: 200px');

//Step 1B : Prepare caption sub component. e.g - figcaption.

let imageTitleTextContainer = document.createElement('figcaption');

let imageTitleText = document.createTextNode('Beautiful Image');

imageTitleTextContainer.appendChild(imageTitleText);

//Step 1C : Prepare link sub component . e.g - a tag.

let imageLinkCommentContainer = document.createElement('a');

let imageLinkComment = document.createTextNode('Click Here');

imageLinkCommentContainer.appendChild(imageLinkComment);

let imageSubComponentContainer = document.createElement('figure');

//Step 1D : Prepare photo container . e.g - div.

let imageMainContainer = document.createElement('div');

//Step 1D1 : Inserting photo component into photo container. e.g - inserting figure into div.

//Step 1D2 : Inserting caption comment into photo container. e.g - inserting fig caption into figure.

//Step 1D3 : Inserting link comment into photo container . e.g - inserting a tag into div.
imageSubComponentContainer.appendChild(imageContainer);
imageSubComponentContainer.appendChild(imageTitleTextContainer);
imageSubComponentContainer.appendChild(imageLinkCommentContainer);
imageMainContainer.appendChild(imageSubComponentContainer);



//Step 2 : Repeat preparation of photo component.

//Step 2A : Inserting virtual dom into tree.


let htmlLocation = document.querySelector('#root');

//Step 5B : Insert all posts into targeted HTML tree.
htmlLocation.appendChild(imageMainContainer);