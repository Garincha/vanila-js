'use strict';


/* * Objective : Displaying Multiple comments into a post by comment JSON (API)



0 : Collecting comments data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .
*/

const John = {};

John.BlogComments = {
    //1 : Preparing Commenter Name 
getCommenterName : function(name){

    let commenterName = null;

    //Step 1A : Creating container for commenter name . e.g - using em node
    let commenterNameContainer = document.createElement('em');

    //Step 1B : Creating text node.
    let commenterNameText = document.createTextNode(name);

    //Step 1C : Inserting text node into container node. e.g - inside em node
    commenterNameContainer.appendChild(commenterNameText);

    commenterName = commenterNameContainer;//(content + container)

    return commenterName;

},

//2 : Preparing Commenter Email  
getCommenterEmail : function(email){

    let commenterEmail = null;

    //Step 2A : Creating container for commenter email . e.g - strong node
    let commenterEmailContainer = document.createElement('strong');

    //Step 2B : Creating text node .
    let commenterEmailText = document.createTextNode(email);

    //Step 2C : Inserting text into container node.
    commenterEmailContainer.appendChild(commenterEmailText);

    commenterEmail = commenterEmailContainer;//(content + container)

    return commenterEmail;

},

//3 : Preparing Comments Body
getCommentBody : function(body) {
    
    let commentBody = null;

    //Step 3A : Creating container for comment body/detail/description. e.g - span node.
    let commentBodyContainer = document.createElement('span');

    //Step 3B : Creating text node for body/description content.
    let commentBodyText = document.createTextNode(body);

    //Step 3C : Inserting text into comment detail container . e.g - inside span node.
    commentBodyContainer.appendChild(commentBodyText);

    commentBody = commentBodyContainer;

    return commentBody;

},

    //4 : Preparing Single Comment

getComment : function(commentData) {

    let comment = null;
    //Step 4A : Creating comment container. e.g - using p node
    let commentContainer = document.createElement('p');

    //Step 4B : Inserting  commenter name (1), comments body (2), commenter email (3) into comment container .
    let commenterName = this.getCommenterName(commentData.name);
    let commenterEmail = this.getCommenterEmail(commentData.email);
    let commentBody = this.getCommentBody(commentData.body);
    commentContainer.appendChild(commenterName);
    commentContainer.appendChild(commenterEmail);
    commentContainer.appendChild(commentBody);

    comment = commentContainer;

    return comment;
},

display : function(Location,commentsData) {
    //Step 5A : Target the Location
    let htmlLocation = document.querySelector(Location);

    for(let commentData of commentsData){
        let comment = this.getComment(commentData);
        htmlLocation.appendChild(comment);
        console.log(comment);
    }
    
   //console.log(htmlLocation); 
}
};








//Step 4C : Creating another comment container . e.g - using div node.

//Step 4D : Inserting all nodes into container. e.g - inside div node.

//5 : Preparing All Comments

//Step 5A : Repeat 1,2,3,4 .

//let comment = getComment();

//5 : Display the Comment




//Step 5B : Insert all Comments into targeted HTML tree.
//htmlLocation.appendChild(comment);

function loadComments() {
    fetch('https://jsonplaceholder.typicode.com/comments')
        .then(response => response.json())
        .then(function(comments){
        //console.log(posts)
        //display('#root', comments);
        John.BlogComments.display('#root',comments);
  });
}
