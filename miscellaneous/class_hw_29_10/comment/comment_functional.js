'use strict';


/* * Objective : Displaying Multiple comments into a post by comment JSON (API)



0 : Collecting comments data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .
*/
//1 : Preparing Commenter Name 
function getCommenterName(){

    let commenterName = null;

    //Step 1A : Creating container for commenter name . e.g - using em node
    let commenterNameContainer = document.createElement('em');

    //Step 1B : Creating text node.
    let commenterNameText = document.createTextNode('Commenter');

    //Step 1C : Inserting text node into container node. e.g - inside em node
    commenterNameContainer.appendChild(commenterNameText);

    commenterName = commenterNameContainer;//(content + container)

    return commenterName;

}
//2 : Preparing Commenter Email  
function getCommenterEmail(){

    let commenterEmail = null;

    //Step 2A : Creating container for commenter email . e.g - strong node
    let commenterEmailContainer = document.createElement('strong');

    //Step 2B : Creating text node .
    let commenterEmailText = document.createTextNode('Email');

    //Step 2C : Inserting text into container node.
    commenterEmailContainer.appendChild(commenterEmailText);

    commenterEmail = commenterEmailContainer;//(content + container)

    return commenterEmail;

}

//3 : Preparing Comments Body
function getCommentBody() {
    
    let commentBody = null;

    //Step 3A : Creating container for comment body/detail/description. e.g - span node.
    let commentBodyContainer = document.createElement('span');

    //Step 3B : Creating text node for body/description content.
    let commentBodyText = document.createTextNode('Comments Body');

    //Step 3C : Inserting text into comment detail container . e.g - inside span node.
    commentBodyContainer.appendChild(commentBodyText);

    commentBody = commentBodyContainer;

    return commentBody;

}


//4 : Preparing Single Comment

function getComment() {

    let comment = null;
    //Step 4A : Creating comment container. e.g - using p node
    let commentContainer = document.createElement('p');

    //Step 4B : Inserting  commenter name (1), comments body (2), commenter email (3) into comment container .
    let commenterName = getCommenterName();
    let commenterEmail = getCommenterEmail();
    let commentBody = getCommentBody();
    commentContainer.appendChild(commenterName);
    commentContainer.appendChild(commenterEmail);
    commentContainer.appendChild(commentBody);

    comment = commentContainer;

    return comment;
}


//Step 4C : Creating another comment container . e.g - using div node.

//Step 4D : Inserting all nodes into container. e.g - inside div node.

//5 : Preparing All Comments

//Step 5A : Repeat 1,2,3,4 .

let comment = getComment();

//5 : Display the Comment

//Step 5A : Target the Location
let htmlLocation = document.querySelector('#root');

//Step 5B : Insert all Comments into targeted HTML tree.
htmlLocation.appendChild(comment);