* Objective : Displaying Multiple photos from a post JSON (API)



Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .

Step 1 : Prepare photo component . e.g - making a photo.

Step 1A : Prepare image/pictue sub component. e.g - figure tag.

Step 1B : Prepare caption sub component. e.g - figcaption.

Step 1C : Prepare link sub component . e.g - a tag.

Step 1D : Prepare photo container . e.g - div.

Step 1D1 : Inserting photo component into photo container. e.g - inserting figure into div.

Step 1D2 : Inserting caption comment into photo container. e.g - inserting fig caption into figure.

Step 1D3 : Inserting link comment into photo container . e.g - inserting a tag into div.

Step 2 : Repeat preparation of photo component.

Step 2A : Inserting virtual dom into tree.

 

