/*
* Objective : Displaying Multiple todos from a todo JSON (API)



0 : Collecting todo data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .


//1 : Preparing todo item 1

//Step 1A : Creating container for todo item1 . e.g - using li node.
let todoComponentContainer = document.createElement('li');

//Step 1B : Creating another container for todo item1 . e.g - using input node. 
let todoSubComponentContainer = document.createElement('input');

//Step 1C : Set attribute into input node. e.g - using checkbox 
todoSubComponentContainer.setAttribute('custom-checkbox','');

//Step 1C : Creating another more container for todo item1 . e.g - using span node.
let todoSubComponentContainer2 = document.createElement('span');

//Step 1D : Creating text node for todo item1.
let todoText = document.createTextNode('Todo Item');

//Step 1E : Inserting text node into container node. e.g -span
todoSubComponentContainer2.appendChild(todoText);

2 . Preparing all todo items

Step 2A : Repeat 1.

//3 : Preparing Single Todo 

//Step 3A : Creating Todo container. e.g - using ul
let todoContainer = document.createElement('ul');

//Step 3B : Inserting all todo items into Todo container .

4 : Preparing All Todos

Step 4A : Repeat 1,2,3 .

5 : Display the Todo

//Step 5A : Target the Location
let htmlLocation = document.querySelector('#root');

//Step 5B : Insert all Todos into targeted HTML tree.
htmlLocation.appendChild(todoMainContainer);
//Step 1B : Prepare caption sub component. e.g - figcaption.

*/
"use strict"

/*
* Objective : Displaying Multiple todos from a todo JSON (API)

0 : Collecting todos data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .
*/

const John = {};

John.Todos = {

    getTodoId : function(id){
         
        let todoId = null;

        //step 1 : creating text node.
        let todoIdText = document.createTextNode("Id" + id);

        //step 2 : creating container element 
        let todoIdContainer = document.createElement('li');

        //step 3 : Inserting text node into container element
        todoIdContainer.appendChild(todoIdText);
        todoId = todoIdContainer;

        return todoId;
    },

    getTodoTitle : function(title){

        let todoTitle = null;

        //step 1 : creating text node
        let todoTitelText = document.createTextNode(title);

        //step 2 : creating container element
        let todoTitleContainer = document.createElement('li');
        let input = document.createElement('input');
        input.setAttribute('type','checkbox');
        let textContainer = document.createElement('span');
        textContainer.appendChild(todoTitelText);

        //step 3 : Inserting text node inside element (li)
        todoTitleContainer.appendChild(input);
        todoTitleContainer.appendChild(textContainer);

        todoTitle = todoTitleContainer;
        return todoTitle;
    },

    getTodoBody : function(body){
        let todoBody = null;

        //step 4 : creating text node
        let todoBodyText = document.createTextNode(body);

        //step 5 : creating element(li)
        let todoBodyContainer = document.createElement('li');
        let input = document.createElement('input');
        input.setAttribute('type','checkbox');
        let span = document.createElement('span');
        span.appendChild(todoBodyText);

        //step 6 : Inserting text node inside element(li)

        todoBodyContainer.appendChild(input);
        todoBodyContainer.appendChild(span);

        todoBody = todoBodyContainer;

        return todoBody;
    },

    getTodo : function(todoData){
        let todo = null;

        //step 7 : creating element (ul cum container)
        let todoContainer = document.createElement('ul');
        let todoTitle = this.getTodoTitle(todoData.title);
        let todoBody = this.getTodoBody(todoData.completed);
        let todoId = this.getTodoId(todoData.id);

        //step 8 : Inserting todo id, title & todo body into container
        todoContainer.appendChild(todoId);
        todoContainer.appendChild(todoTitle);
        todoContainer.appendChild(todoBody);

        todo = todoContainer;

        todo.setAttribute('style','border : 1px solid #dEd4d1; width : 600px; margin : 20px auto; padding : 50px');

        return todo;
    },

    display : function(Location,todosData){
        //targeting html location
        let htmlLocation = document.querySelector(Location);
        for(let todoData of todosData){
            let todo = this.getTodo(todoData);
            htmlLocation.appendChild(todo);
        }
    },
};


function loadTodos(){
    fetch('https://jsonplaceholder.typicode.com/todos')
        .then(response => response.json())
        .then(function(todos){
             //console.log(posts)
             John.Todos.display('#root', todos);
            //console.log(window);
            //window.data = posts;
  });

}