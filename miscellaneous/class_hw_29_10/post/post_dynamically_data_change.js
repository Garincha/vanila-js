/*
* Objective : Displaying Multiple posts from a post JSON (API)

0 : Collecting posts data 

4 : Preparing All Posts 
    3 : Preparing Single Post
	1 : Preparing Post Title
		title container
		title text
	2 : Preparing Post Body
		body container
		body text
	........repeat

.............................................

0 : Collecting posts data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .
*/

//1 : Preparing Post Title 
function getPostTitle(title) {// here, within this function Post Title Argument sits as title parameter & passing data/value
    
    let postTitle = null;

    //Step 1A : Creating container for post title . e.g - using h1 node.
    let postTitleContainer = document.createElement('h1');

    //Step 1B : Creating text node.
    let postTitleText = document.createTextNode(title);

    //Step 1C : Inserting text node into container node.
    postTitleContainer.appendChild(postTitleText);

    postTitle = postTitleContainer;//(container + content)

    return postTitle;

}
//2 : Preparing Post Body
function getPostBody(body){// here, within this function Post Body Argument sits as body parameter & passing data/value
    

    let postBody = null;

    //Step 2A : Creating container for post body/detail/description. e.g - p node.
    let postBodyContainer = document.createElement('p');


    //Step 2B : Creating text node for body/description content.
    let postBodyText = document.createTextNode(body);

    //Step 2C : Inserting text into post detail container .
    postBodyContainer.appendChild(postBodyText);

    postBody = postBodyContainer;//(container + content)

    return postBody;

}
//3 : Preparing Single Post 
function getPost() {
    
    let post = null;

    //Step 3A : Creating post container. e.g - using div
    let postContainer = document.createElement('div');

    let postTitle = getPostTitle('Post Title Argument');// calling function getPostTitle & passing an argument(Post Title Argument) as a data. 
    let postBody = getPostBody('Post Body Argument');// calling function getPostBody & passing an argument(Post Body Argument) & as a data. 
    //Step 3B : Insert post title (1) and post body (2) into post container .
    postContainer.appendChild(postTitle);
    postContainer.appendChild(postBody);

    post = postContainer; //(container + content)

    return post;

}

let post = getPost();
//4 : Preparing All Posts 

//Step 4A : Repeat 1,2,3 .

let post2 = getPost();
let post3 = getPost();


//5 : Display the Post (populate into html tree)

//Step 5A : Target the Location
let htmlLocation = document.querySelector('#root');

//Step 5B : Insert all posts into targeted HTML tree.
htmlLocation.appendChild(post).appendChild(post2).appendChild(post3);