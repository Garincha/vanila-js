"use strict"
/*
* Objective : Displaying Multiple posts from a post JSON (API)

0 : Collecting posts data 

4 : Preparing All Posts 
    3 : Preparing Single Post
	1 : Preparing Post Title
		title container
		title text
	2 : Preparing Post Body
		body container
		body text
	........repeat

.............................................

0 : Collecting posts data 

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .
*/

const John = {}// this is a top level packet/object

John.BlogPosts = {// this is a property of John 


//1 : Preparing Post Title 
 getPostTitle : function(title) {// here, within this function Post Title Argument sits as title parameter & passing data/value
 //getPostTitle is a method of John.BlogPosts object   
    let postTitle = null;

    //Step 1A : Creating container for post title . e.g - using h1 node.
    let postTitleContainer = document.createElement('h1');

    //Step 1B : Creating text node.
    let postTitleText = document.createTextNode(title);

    //Step 1C : Inserting text node into container node.
    postTitleContainer.appendChild(postTitleText);

    postTitle = postTitleContainer;//(container + content)

    return postTitle;

},



//2 : Preparing Post Body
 getPostBody: function(body){// here, within this function Post Body Argument sits as body parameter & passing data/value
    

    let postBody = null;

    //Step 2A : Creating container for post body/detail/description. e.g - p node.
    let postBodyContainer = document.createElement('p');


    //Step 2B : Creating text node for body/description content.
    let postBodyText = document.createTextNode(body);

    //Step 2C : Inserting text into post detail container .
    postBodyContainer.appendChild(postBodyText);

    postBody = postBodyContainer;//(container + content)

    return postBody;

},

//3 : Preparing Single Post 
getPost : function(postData){// here, within this function title & body sits as parameter's & passing data/value of getPost functions arguments(content for body,content for title)
    
    let post = null;

    //Step 3A : Creating post container. e.g - using div
    let postContainer = document.createElement('div');

    let postTitle = this.getPostTitle(postData.title);// calling function getPostTitle & passing an argument(title) as a data, which we're getting by getPost() functions parameter(title). 
    let postBody = this.getPostBody(postData.body);// calling function getPostBody & passing and argument(body) as a data, which we're getting by getPost() functions parameter(body). 
    //Step 3B : Insert post title (1) and post body (2) into post container .
    postContainer.appendChild(postTitle);
    postContainer.appendChild(postBody);

    post = postContainer; //(container + content)

    return post;

},

display : function(Location,postsData) {

    //Step 5A : Target the Location
    let htmlLocation = document.querySelector(Location);

    for(let postData of postsData){
        let post = this.getPost(postData);
        htmlLocation.appendChild(post);
        console.log(post);
    }
    
   console.log(htmlLocation); 
},


};
/*
window.data = null;
 John.RemoteData = {
    data : null,
    retrieve : function(){
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(function(posts){
             //console.log(posts)
            //John.BlogPosts.display('#root', posts);
            //console.log(window);
            window.data = posts;
  });
    }
};


John.RemoteData.retrieve();
console.log(window.data);
//4 : Preparing All Posts 
*/
//Step 4A : Repeat 1,2,3 .




//5 : Display the Post (populate into html tree)





/*
for(let postDataIndex in postsData){
    let post = getPost(postsData[postDataIndex].title,postsData[postDataIndex].body);
    htmlLocation.appendChild(post);
    console.log(postDataIndex);
}
*/


//let postsData = null;
function loadPosts(){
    fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(function(posts){
             //console.log(posts)
            John.BlogPosts.display('#root', posts);
            //console.log(window);
            //window.data = posts;
  });

}
//display('#root',postsData);//calling the function with two arguments. 


//Step 5B : Insert all posts into targeted HTML tree.

/*
let post2 = getPost(postsData[1].title,postsData[1].body);
htmlLocation.appendChild(post2);

let post3 = getPost(postsData[2].title,postsData[2].body);
htmlLocation.appendChild(post3);

let post4 = getPost(postsData[3].title,postsData[3].body);
htmlLocation.appendChild(post4);
*/

