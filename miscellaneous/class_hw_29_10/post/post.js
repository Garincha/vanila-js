'use strict';


let data = (function () {
        const posts = [
            {

                //"userId": 1,
                //"id": 1,
                "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
            
            },
            /*{
            
                "userId": 1,
                "id": 2,
                "title": "qui est esse",
                "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
            
            },
            {
            
                "userId": 1,
                "id": 3,
                "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",
                "body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
            
            },
            {
            
                "userId": 1,
                "id": 4,
                "title": "eum et est occaecati",
                "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
            
            },
            {
            
                "userId": 1,
                "id": 5,
                "title": "nesciunt quas odio",
                "body": "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
            
            },*/
        ];  

    return posts;
})();//IIFE 

const John = {};

John.htmlPost = {
    displayTitle : function(title){
        let titleText = document.createTextNode(data.title);
        let h2 = document.createElement('h2');
        h2.appendChild(titleText);
        //img.setAttribute('src',userImg);
        //img.setAttribute('style','width: 200px; height: 200px');
        return h2;
    },
    displayBody : function (body) {
        let bodyText = document.createTextNode(data.body);
        let p = document.createElement('p');
        p.appendChild(bodyText);
        return p;
    },
    preparePost : function (data) {
        let post = document.createElement('div');
        let postTitle = '';
        let postBody = '';
        
        postTitle = this.displayTitle(data);
        post.appendChild(postTitle);
        postBody = this.displayBody(data);
        post.appendChild(postBody);
        //post.setAttribute('style','display : inline-block');
        return post;
    },
    display : function (data, domLocaation) {
        let container = document.querySelector(domLocaation);
        let showPost = this.preparePost(data);
        container.appendChild(showPost);
        
        return true;
    }
};

John.htmlPost.display(data,'#root');


