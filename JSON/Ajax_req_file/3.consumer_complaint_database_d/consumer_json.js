"use strict";

const John = {};

John.ConsumerData = {

    getConsumerId : function (Id) {
        
        let consumerId = null;

        let consumerIdContainer = document.createElement('li');

        let consumerIdText = document.createTextNode(Id);

        consumerIdContainer.appendChild(consumerIdText);

        consumerId = consumerIdContainer;
        console.log(consumerId);
        return consumerId;
    },

    getConsumerName : function (Name) {
        
        let consumerName = null;

        let consumerNameContainer = document.createElement('li');

        let consumerNameText = document.createTextNode(Name);

        consumerNameContainer.appendChild(consumerNameText);

        consumerName = consumerNameContainer;
        console.log( consumerName);
        return  consumerName;
    },

    getConsumerAverageRating : function (AveRating) {
        
        let consumerAverageRating = null;

        let consumerAverageRatingContainer = document.createElement('li');

        let consumerAverageRatingText = document.createTextNode(AveRating);

        consumerAverageRatingContainer.appendChild(consumerAverageRatingText);

        consumerAverageRating = consumerAverageRatingContainer;
        console.log(consumerAverageRating);
        return  consumerAverageRating;
    }, 
    
    getConsumerCreatedAt : function (createdAt) {
        
        let consumerCreatedAt = null;

        let consumerCreatedAtContainer = document.createElement('li');

        let consumerCreatedAtText = document.createTextNode(createdAt);

        consumerCreatedAtContainer.appendChild(consumerCreatedAtText);

        consumerCreatedAt = consumerCreatedAtContainer;
        console.log(consumerCreatedAt);
        return  consumerCreatedAt;
    }, 

    getConsumerDescription : function (description) {
        
        let consumerDescription = null;

        let consumerDescriptionContainer = document.createElement('li');

        let consumerDescriptionText = document.createTextNode(description);

        consumerDescriptionContainer.appendChild(consumerDescriptionText);

        consumerDescription = consumerDescriptionContainer;
        console.log(consumerDescription);
        return  consumerDescription;
    },

    getConsumerPublicationStage : function (pubStage) {
        
        let consumerPublicationStage = null;

        let consumerPublicationStageContainer = document.createElement('li');

        let consumerPublicationStageText = document.createTextNode(pubStage);

        consumerPublicationStageContainer.appendChild(consumerPublicationStageText);

        consumerPublicationStage = consumerPublicationStageContainer;
        console.log(consumerPublicationStage);
        return  consumerPublicationStage;
    },

    getConsumerBody : function () {
        
        let consumerBody = null;

        let consumerBodyContainer = document.createElement('ul');

        consumerBody = consumerBodyContainer;
        console.log(consumerBody);
        return consumerBody;
    },

    getConsumer : function(consumerData){
    
        let consumer = null;
    
        let consumerMainContainer = document.createElement('div');
        //console.log();
        let consumerId = this.getConsumerId(consumerData.id);
        let consumerName = this.getConsumerName(consumerData.name);
        let consumerAverageRating = this.getConsumerAverageRating(consumerData.averageRating);
        let consumerCreatedAt = this.getConsumerCreatedAt(consumerData.createdAt);
        let consumerDescription = this.getConsumerDescription(consumerData.description);
        let consumerPublicationStage = this.getConsumerPublicationStage(consumerData.publicationStage);
        let getConsumerBody = this.getConsumerBody(consumerData.body);
        consumerMainContainer.appendChild(consumerId);
        consumerMainContainer.appendChild(consumerName);
        consumerMainContainer.appendChild(consumerAverageRating);
        consumerMainContainer.appendChild(consumerCreatedAt);
        consumerMainContainer.appendChild(consumerDescription);
        consumerMainContainer.appendChild(consumerPublicationStage);
        consumerMainContainer.appendChild(getConsumerBody);
     
    
        consumer = consumerMainContainer; //(container + content)
    
        return consumer;
    
    },

    display : function (Location,consumersData) {
        let htmlLocation = document.querySelector(Location);

        for(let consumerData of consumersData){
            let consumer = this.getConsumer(consumerData);
            htmlLocation.appendChild(consumer);
            //console.log(book);
    }
},

};

function loadconsumers(){
    fetch('http://data.consumerfinance.gov/api/views.json')
        .then(response => response.json())
        .then(function(consumers){
             //console.log(books);
             John.ConsumerData.display('#root',consumers);
            //console.log(window);
            //window.data = posts;
  });

}