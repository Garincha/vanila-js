"use strict";

const John = {};

John.FruitsData = {
    
    getFruitsId : function (Id) {
        
        let fruitsId = null;

        let fruitsIdContainer = document.createElement('li');

        let fruitsIdText = document.createTextNode(Id);

        fruitsIdContainer.appendChild(fruitsIdText);

        fruitsId = fruitsIdContainer;
        console.log(fruitsId);
        return fruitsId;
    },

    getFruitsName : function (Name) {
        
        let fruitsName = null;

        let fruitsNameContainer = document.createElement('li');

        let fruitsNameText = document.createTextNode(Name);

        fruitsNameContainer.appendChild(fruitsNameText);

        fruitsName = fruitsNameContainer;
        console.log(fruitsName);
        return fruitsName;
    },

    getFruitsProviderId : function (providerId) {
        
        let fruitsProviderId = null;

        let fruitsProviderContainer = document.createElement('li');

        let fruitsProviderText = document.createTextNode(providerId);

        fruitsProviderContainer.appendChild(fruitsProviderText);

        fruitsProviderId  = fruitsProviderContainer;
        console.log(fruitsProviderId);
        return fruitsProviderId;
    },

    getFruitsDescription : function (providerId) {
        
        let fruitsDescription = null;

        let fruitsDescriptionContainer = document.createElement('li');

        let fruitsDescriptionText = document.createTextNode(providerId);

        fruitsDescriptionContainer.appendChild(fruitsDescriptionText);

        fruitsDescription  = fruitsDescriptionContainer;
        console.log(fruitsDescription);
        return fruitsDescription;
    },

    getFruitsBody : function (fruitData) {
        
        let fruitBody = null;

        let fruitBodyContainer = document.createElement('ul');

        fruitBody = fruitBodyContainer;
        console.log(fruitBody);
        return fruitBody;
    },

    getFruit : function(fruitData){
    
        let fruit = null;
    
        let fruitMainContainer = document.createElement('div');
        //console.log();
        let fruitsId = this.getFruitsId(fruitData.fruits.id);
        let fruitsName = this.getFruitsName(fruitData.fruits.name);
        let fruitsProviderId = this.getFruitsProviderId(fruitData.fruits.providerId);
        let fruitsDescription = this.getFruitsDescription(fruitData.fruits.description);
        let fruitsBody = this.getFruitsBody(fruitData.fruits.body);
        fruitMainContainer.appendChild(fruitsId);
        fruitMainContainer.appendChild(fruitsName);
        fruitMainContainer.appendChild(fruitsProviderId);
        fruitMainContainer.appendChild(fruitsDescription);
        fruitMainContainer.appendChild(fruitsBody);
     
    
        fruit = fruitMainContainer; //(container + content)
    
        return fruit;
    
    },

    display : function (Location,fruitsData) {
        let htmlLocation = document.querySelector(Location);

        for(let fruitData of fruitsData){
            let fruit = this.getFruit(fruitData);
            htmlLocation.appendChild(fruit);
            //console.log(book);
    }

    },

};

function loadFruits(){
    fetch('https://gist.githubusercontent.com/theGlenn/f6382243307292fe83831da273e4aa48/raw/88a830d334e8411d28e689e029a03dcc4bf0f4ae/fruits.data.json')
        .then(response => response.json())
        .then(function(fruits){
             //console.log(fruits);
             John.FruitsData.display('#root',fruits);
            //console.log(window);
            //window.data = posts;
  });
}