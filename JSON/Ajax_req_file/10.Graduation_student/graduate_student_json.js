"use strict";

const John = {};

John.GraduateStudentData = {

    getStudentFullName : function (name) {
        
        let studentFullName = null;

        let studentFullNameContainer = document.createElement('li');

        let studentFullNameText = document.createTextNode(name);

        studentFullNameContainer.appendChild(studentFullNameText);

        studentFullName = studentFullNameContainer;
        console.log(studentFullName);
        return studentFullName;
    },

    getStudentsQualifiedSchoolName : function (schoolName) {
        
        let studentsQualifiedSchoolName = null;

        let studentsQualifiedSchoolNameContainer = document.createElement('li');

        let studentsQualifiedSchoolNameText = document.createTextNode(schoolName);

        studentsQualifiedSchoolNameContainer.appendChild(studentsQualifiedSchoolNameText);

        studentsQualifiedSchoolName = studentsQualifiedSchoolNameContainer;
        console.log(studentsQualifiedSchoolName);
        return studentsQualifiedSchoolName;
    },

    getStudentEmail : function (email) {
        
        let studentEmail = null;

        let studentEmailContainer = document.createElement('li');

        let studentEmailText = document.createTextNode(email);

        studentEmailContainer.appendChild(studentEmailText);

        studentEmail = studentEmailContainer;
        console.log(studentEmail);
        return studentEmail;
    },

    getStudentfullGradDate : function (gradeDate) {
        
        let studentfullGradDate = null;

        let studentfullGradDateContainer = document.createElement('li');

        let studentfullGradDateText = document.createTextNode(gradeDate);

        studentfullGradDateContainer.appendChild(studentfullGradDateText);

        studentfullGradDate = studentfullGradDateContainer;
        console.log(studentfullGradDate);
        return studentfullGradDate;
    },

    getStudentMajorSubject : function (majorSubject) {
        
        let studentMajorSubject = null;

        let studentMajorSubjectContainer = document.createElement('li');

        let studentMajorSubjectText = document.createTextNode(majorSubject);

        studentMajorSubjectContainer.appendChild(studentMajorSubjectText);

        studentMajorSubject = studentMajorSubjectContainer;
        console.log(studentMajorSubject);
        return studentMajorSubject;
    },

    getStudentsSecondMajorSubject : function (secondMajor) {
        
        let studentsSecondMajorSubject = null;

        let studentsSecondMajorSubjectContainer = document.createElement('li');

        let studentsSecondMajorSubjectText = document.createTextNode(secondMajor);

        studentsSecondMajorSubjectContainer.appendChild(studentsSecondMajorSubjectText);

        studentsSecondMajorSubject = studentsSecondMajorSubjectContainer;
        console.log(studentsSecondMajorSubject);
        return studentsSecondMajorSubject;
    },

    getStudentBody : function () {
    
        let studentBody = null;
    
        let studentBodyContainer = document.createElement('div');
    
        studentBody = studentBodyContainer;
    
        return studentBody;
    },

    getGraduateStudent : function(studentData){
    
        let graduateStudent = null;
    
        let graduateStudentMainContainer = document.createElement('div');
        //console.log();
        let studentFullName = this.getStudentFullName(studentData.fullName);
        let studentsQualifiedSchoolName = this.getStudentsQualifiedSchoolName(studentData.qualifiedSchoolName);
        let studentEmail = this.getStudentEmail(studentData.email);
        let studentfullGradDate = this.getStudentfullGradDate(studentData.fullGradDate);
        let studentMajorSubject = this.getStudentMajorSubject(studentData.major);
        let studentsSecondMajorSubject = this.getStudentsSecondMajorSubject(studentData.secondMajor);
        let studentBody  = this.getStudentBody(studentData.body);
        graduateStudentMainContainer.appendChild(studentFullName);
        graduateStudentMainContainer.appendChild(studentsQualifiedSchoolName);
        graduateStudentMainContainer.appendChild(studentEmail);
        graduateStudentMainContainer.appendChild(studentfullGradDate);
        graduateStudentMainContainer.appendChild(studentMajorSubject);
        graduateStudentMainContainer.appendChild(studentsSecondMajorSubject);
        graduateStudentMainContainer.appendChild(studentBody);
        
        graduateStudent = graduateStudentMainContainer; //(container + content)
    
        return graduateStudent;
    
    },

    display : function (Location,studentsData) {
        let htmlLocation = document.querySelector(Location);

        for(let studentData of studentsData){
            let student = this.getGraduateStudent(studentData);
            htmlLocation.appendChild(student);
            //console.log(book);
    }
},

};

function loadStudentsData(){
    fetch('https://gist.githubusercontent.com/vinayh/325c540dc01fef062624/raw/ab68cee0a83ed60e2a5f384a577043df735810ec/data.json')
        .then(response => response.json())
        .then(function(students){
             console.log(students);
             John.GraduateStudentData.display('#root',students);
            //console.log(window);
            //window.data = posts;
  });

}