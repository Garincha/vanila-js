"use strict";

const John = {};

John.ParseDate = {

    getParseJsonId : function (id) {
        
        let parseJsonId = null;

        let parseJsonIdContainer = document.createElement('li');

        let parseJsonIdText = document.createTextNode(id);

        parseJsonIdContainer.appendChild(parseJsonIdText);

        parseJsonId = parseJsonIdContainer;
        console.log(parseJsonId);
        return parseJsonId;
    },

    getParseJsonFirstName : function (firstName) {
        
        let parseJsonFirstName = null;

        let parseJsonFirstNameContainer = document.createElement('li');

        let parseJsonFirstNameText = document.createTextNode(firstName);

        parseJsonFirstNameContainer.appendChild(parseJsonFirstNameText);

        parseJsonFirstName = parseJsonFirstNameContainer;
        console.log(parseJsonFirstName);
        return parseJsonFirstName;
    },

    getParseJsonLastName : function (lastName) {
        
        let parseJsonLastName = null;

        let parseJsonLastNameContainer = document.createElement('li');

        let parseJsonLastNameText = document.createTextNode(lastName);

        parseJsonLastNameContainer.appendChild(parseJsonLastNameText);

        parseJsonLastName = parseJsonLastNameContainer;
        console.log(parseJsonLastName);
        return parseJsonLastName;
    },

    getParseJsonEmail : function (email) {
        
        let parseJsonEmail = null;

        let parseJsonEmailContainer = document.createElement('li');

        let parseJsonEmailText = document.createTextNode(email);

        parseJsonEmailContainer.appendChild(parseJsonEmailText);

        parseJsonEmail = parseJsonEmailContainer;
        console.log(parseJsonEmail);
        return parseJsonEmail;
    },

    getParseJsonEmail : function (email) {
        
        let parseJsonEmail = null;

        let parseJsonEmailContainer = document.createElement('li');

        let parseJsonEmailText = document.createTextNode(email);

        parseJsonEmailContainer.appendChild(parseJsonEmailText);

        parseJsonEmail = parseJsonEmailContainer;
        console.log(parseJsonEmail);
        return parseJsonEmail;
    },

    getParseJsonGender : function (gender) {
        
        let parseJsonGender = null;

        let parseJsonGenderContainer = document.createElement('li');

        let parseJsonGenderText = document.createTextNode(gender);

        parseJsonGenderContainer.appendChild(parseJsonGenderText);

        parseJsonGender = parseJsonGenderContainer;
        console.log(parseJsonGender);
        return parseJsonGender;
    },

    getParseJsonIpAddress : function (IpAddress) {
        
        let parseJsonIpAddress = null;

        let parseJsonIpAddressContainer = document.createElement('li');

        let parseJsonIpAddressText = document.createTextNode(IpAddress);

        parseJsonIpAddressContainer.appendChild(parseJsonIpAddressText);

        parseJsonIpAddress = parseJsonIpAddressContainer;
        console.log(parseJsonIpAddress);
        return parseJsonIpAddress;
    },

    getParseJsonBody : function () {
    
        let parseJsonBody = null;
    
        let parseJsonBodyContainer = document.createElement('div');
    
        parseJsonBody = parseJsonBodyContainer;
    
        return parseJsonBody;
    },

    getParseJson : function(parseData){
    
        let parseJson = null;
    
        let parseJsonMainContainer = document.createElement('div');
        //console.log();
        let parseJsonId = this.getParseJsonId(parseData.id);
        let parseJsonFirstName = this.getParseJsonFirstName(parseData.first_name);
        let parseJsonLastName = this.getParseJsonLastName(parseData.last_name);
        let parseJsonEmail = this.getParseJsonEmail(parseData.email);
        let parseJsonGender = this.getParseJsonGender(parseData.gender);
        let parseJsonIpAddress = this.getParseJsonIpAddress(parseData.ip_address);
        let parseJsonBody  = this.getParseJsonBody(parseData.body);
        parseJsonMainContainer.appendChild(parseJsonId);
        parseJsonMainContainer.appendChild(parseJsonFirstName);
        parseJsonMainContainer.appendChild(parseJsonLastName);
        parseJsonMainContainer.appendChild(parseJsonEmail);
        parseJsonMainContainer.appendChild(parseJsonGender);
        parseJsonMainContainer.appendChild(parseJsonIpAddress);
        parseJsonMainContainer.appendChild(parseJsonBody);
        
        parseJson = parseJsonMainContainer; //(container + content)
    
        return parseJson;
    
    },

    display : function (Location,parsesData) {
        let htmlLocation = document.querySelector(Location);

        for(let parseData of parsesData){
            let parse = this.getParseJson(parseData);
            htmlLocation.appendChild(parse);
            //console.log(book);
    }
},

};

function loadparsesData(){
    fetch('https://gist.githubusercontent.com/zerdnem/f05a30a38b03be7bd504bf840e559e44/raw/6ac39f5a18681e671a7c522454e6b2aae182bdc3/data.json')
        .then(response => response.json())
        .then(function(parses){
             console.log(parses);
             John.ParseDate.display('#root',parses);
            //console.log(window);
            //window.data = posts;
  });

}