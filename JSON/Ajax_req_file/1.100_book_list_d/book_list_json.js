"use strict";

const John = {};

John.BookLists = {

    getBookImageSource : function(imageUrl) {
    
        let bookImageSource = null;
    
        let imageContainer = document.createElement('img');
    
        imageContainer.setAttribute('src',imageUrl);
    
        imageContainer.setAttribute('style','width: 200px; height: 200px');
    
        bookImageSource = imageContainer;
        console.log(bookImageSource);
        return bookImageSource;
    
    },

    getBookAuthortName : function (authorName) {
        
        let bookAuthorName = null;

        let authorNamrContainer = document.createElement('li');

        let authorNameText = document.createTextNode(authorName);

        authorNamrContainer.appendChild(authorNameText);

        bookAuthorName = authorNamrContainer;
        console.log(bookAuthorName);
        return bookAuthorName;
    },

    getBookAuthorCountry : function (authorCountry) {

        let bookAuthorCountry = null;

        let authorCountryContainer = document.createElement('li');

        let authorCountrytext = document.createTextNode(authorCountry);

        authorCountryContainer.appendChild(authorCountrytext);

        bookAuthorCountry = authorCountryContainer;
        console.log(bookAuthorCountry);
        return bookAuthorCountry;
    },

    getBookAuthorLanguage : function (authorLanguage) {
        
        let bookAuthorLanguage = null;

        let authorLanguageContainer = document.createElement('li');

        let authorLanguageText = document.createTextNode(authorLanguage);

        authorLanguageContainer.appendChild(authorLanguageText);

        bookAuthorLanguage = authorLanguageContainer;
        console.log(bookAuthorLanguage);
        return bookAuthorLanguage;
    },

    getBookAuthorLink : function (authorLink) {
        
        let bookAuthorLink = null;

        let authorLinkContainer = document.createElement('li');

        let authorLinkText = document.createTextNode(authorLink);

        authorLinkContainer.appendChild(authorLinkText);

        bookAuthorLink = authorLinkContainer;
        console.log(bookAuthorLink);
        return bookAuthorLink;
    },

    getBookPages : function (pageNumber) {
        
        let bookPages = null;

        let bookPagesContainer = document.createElement('li');

        let bookpagesNumber = document.createTextNode(pageNumber);

        bookPagesContainer.appendChild(bookpagesNumber);

        bookPages = bookPagesContainer;
        console.log(bookPages);
        return bookPages;
    },

    getBookTitle : function (titleText) {
      
        let bookTitle = null;

        let bookTitleContainer = document.createElement('li');

        let bookTitleText = document.createTextNode(titleText);

        bookTitleContainer.appendChild(bookTitleText);

        bookTitle = bookTitleContainer;
        console.log(bookTitle);
        return bookTitle;
    },

    getBookPublishingYear : function (bookYear) {
        let bookPublishingYear = null;

        let publishingYearContainer = document.createElement('li');

        let publishingYear = document.createTextNode(bookYear);

        publishingYearContainer.appendChild(publishingYear);

        bookPublishingYear = publishingYearContainer;
        console.log(bookPublishingYear);
        return bookPublishingYear;
    },

    getBookBody : function () {
        
        let bookBody = null;

        let bookBodyContainer = document.createElement('ul');

        bookBody = bookBodyContainer;
        console.log(bookBody);
        return bookBody;
    },

    getBook : function (bookData) {
      
        let book = null;

        let bookMainContainer = document.createElement('div');

        let imageSource = this.getBookImageSource(bookData.imageLink);

        let bookAuthortName = this.getBookAuthortName(bookData.author);

        let bookAuthorCountry = this.getBookAuthorCountry(bookData.country);

        let bookAuthorLanguage = this.getBookAuthorLanguage(bookData.language);

        let bookAuthorLink = this.getBookAuthorLink(bookData.link);

        let bookPages = this.getBookPages(bookData.pages);

        let bookTitle = this.getBookTitle(bookData.title);

        let bookPublishingYear = this.getBookPublishingYear(bookData.year);

        let bookBody = this.getBookBody(bookData.body);

        bookMainContainer.appendChild(imageSource);
        bookMainContainer.appendChild(bookAuthortName);
        bookMainContainer.appendChild(bookAuthorCountry);
        bookMainContainer.appendChild(bookAuthorLanguage);
        bookMainContainer.appendChild(bookAuthorLink);
        bookMainContainer.appendChild(bookPages);
        bookMainContainer.appendChild(bookTitle);
        bookMainContainer.appendChild(bookPublishingYear);
        bookMainContainer.appendChild(bookBody);

        book = bookMainContainer;
        console.log(book);
        return book;
    },

    display : function (Location,booksData) {
        let htmlLocation = document.querySelector(Location);

        for(let bookData of booksData){
            let book = this.getBook(bookData);
            htmlLocation.appendChild(book);
            //console.log(book);
    }
    },

    
};

function loadBooks(){
    fetch('https://raw.githubusercontent.com/benoitvallon/100-best-books/master/books.json')
        .then(response => response.json())
        .then(function(books){
             //console.log(books);
             John.BookLists.display('#root',books);
            //console.log(window);
            //window.data = posts;
  });

}