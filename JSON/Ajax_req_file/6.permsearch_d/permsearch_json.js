"use strict";

const John = {};

John.PermSearchData = {

    getPermSearchName : function (name) {
        
        let permSearchName = null;

        let permSearchNameContainer = document.createElement('li');

        let permSearchNameText = document.createTextNode(name);

        permSearchNameContainer.appendChild(permSearchNameText);

        permSearchName = permSearchNameContainer;
        console.log(permSearchName);
        return permSearchName;
    },

    getPermSearchFile : function (file) {
        
        let permSearchFile = null;

        let permSearchFileContainer = document.createElement('li');

        let permSearchFileText = document.createTextNode(file);

        permSearchFileContainer.appendChild(permSearchFileText);

        permSearchFile = permSearchFileContainer;
        console.log(permSearchFile);
        return permSearchFile;
    },

    getPermSearchClass : function (permclass) {
        
        let permSearchClass = null;

        let permSearchClassContainer = document.createElement('li');

        let permSearchClassText = document.createTextNode(permclass);

        permSearchClassContainer.appendChild(permSearchClassText);

        permSearchClass = permSearchClassContainer;
        console.log(permSearchClass);
        return permSearchClass;
    },

    getPermSearchCommand : function (command) {
        
        let permSearchCommand = null;

        let permSearchCommandContainer = document.createElement('li');

        let permSearchCommandText = document.createTextNode(command);

        permSearchCommandContainer.appendChild(permSearchCommandText);

        permSearchCommand = permSearchCommandContainer;
        console.log(permSearchCommand);
        return permSearchCommand;
    },

    getPermSearchModule : function (module) {
        
        let permSearchModule = null;

        let permSearchModuleContainer = document.createElement('li');

        let permSearchModuleText = document.createTextNode(module);

        permSearchModuleContainer.appendChild(permSearchModuleText);

        permSearchModule = permSearchModuleContainer;
        console.log(permSearchModule);
        return permSearchModule;
    },

    
    getPermSearchBody : function () {
    
        let permSearchBody = null;
    
        let permSearchBodyContainer = document.createElement('div');
    
        permSearchBody = permSearchBodyContainer;
    
        return permSearchBody;
    },

    getPermSearch : function(permSearchData){
    
        let permSearch = null;
    
        let permSearchMainContainer = document.createElement('div');
        //console.log();
        let permSearchName = this.getPermSearchName(permSearchData.name);
        let permSearchFile = this.getPermSearchFile(permSearchData.file);
        let permSearchClass = this.getPermSearchClass(permSearchData.class);
        let permSearchCommand = this.getPermSearchCommand(permSearchData.command);
        let permSearchModule = this.getPermSearchModule(permSearchData.module);
        let permSearchBody = this.getPermSearchBody(permSearchData.body);

        permSearchMainContainer.appendChild(permSearchName);
        permSearchMainContainer.appendChild(permSearchFile);
        permSearchMainContainer.appendChild(permSearchClass);
        permSearchMainContainer.appendChild(permSearchCommand);
        permSearchMainContainer.appendChild(permSearchModule);
        permSearchMainContainer.appendChild(permSearchBody);

        
        permSearch = permSearchMainContainer; //(container + content)
    
        return permSearch;
    
    },

    display : function (Location,permSearchsData) {
        let htmlLocation = document.querySelector(Location);

        for(let permSearchData of permSearchsData){
            let perm = this.getPermSearch(permSearchData);
            htmlLocation.appendChild(perm);
            //console.log(book);
    }
},

};

function loadpermSearchsData(){
    fetch('https://gist.githubusercontent.com/md678685/859139c33218eec28a242f4f44f0e65b/raw/2e28c3ad94892c65d0f2805a5fce37ac01800a26/permissions.json')
        .then(response => response.json())
        .then(function(permSearchs){
             console.log(permSearchs);
             John.PermSearchData.display('#root',permSearchs);
            //console.log(window);
            //window.data = posts;
  });

}