"use strict";

const John = {};

John.ExhibitData = {

    getExhibitImage : function (imgUrl) {
        
        let exhibitImage = null;

        let exhibitImageContainer = document.createElement('li');

        let exhibitImageText = document.createTextNode(imgUrl);

        exhibitImageContainer.appendChild(exhibitImageText);

        exhibitImage = exhibitImageContainer;
        console.log(exhibitImage);
        return exhibitImage;
    },

    getExhibitName : function (name) {
        
        let exhibitName = null;

        let exhibitNameContainer = document.createElement('li');

        let exhibitNameText = document.createTextNode(name);

        exhibitNameContainer.appendChild(exhibitNameText);

        exhibitName = exhibitNameContainer;
        console.log(exhibitName);
        return exhibitName;
    },

    getExhibitSpecies : function (species) {
        
        let exhibitSpecies = null;

        let exhibitSpeciesContainer = document.createElement('li');

        let exhibitSpeciesText = document.createTextNode(species);

        exhibitSpeciesContainer.appendChild(exhibitSpeciesText);

        exhibitSpecies = exhibitSpeciesContainer;
        console.log(exhibitSpecies);
        return exhibitSpecies;
    },

    

    getExhibitDescription : function (description) {
        
        let exhibitDescription = null;

        let exhibitDescriptionContainer = document.createElement('li');

        let exhibitDescriptionText = document.createTextNode(description);

        exhibitDescriptionContainer.appendChild(exhibitDescriptionText);

        exhibitDescription = exhibitDescriptionContainer;
        console.log(exhibitDescription);
        return exhibitDescription;
    },

    getExhibitThumbnail : function (thumbnailUrl) {
        
        let exhibitThumbnail = null;

        let exhibitThumbnailContainer = document.createElement('li');

        let exhibitThumbnailText = document.createTextNode(thumbnailUrl);

        exhibitThumbnailContainer.appendChild(exhibitThumbnailText);

        exhibitThumbnail = exhibitThumbnailContainer;
        console.log(exhibitThumbnail);
        return exhibitThumbnail;
    },

    
    getExhibitBody : function () {
    
        let exhibitBody = null;
    
        let exhibitBodyContainer = document.createElement('div');
    
        exhibitBody = exhibitBodyContainer;
    
        return exhibitBody;
    },

    getExhibit : function(exhibitData){
    
        let exhibit = null;
    
        let exhibitMainContainer = document.createElement('div');
        //console.log();
        let exhibitImage = this.getExhibitImage(exhibitData.image);
        let exhibitName = this.getExhibitName(exhibitData.name);
        let exhibitSpecies = this.getExhibitSpecies(exhibitData.species);
        let exhibitDescription = this.getExhibitDescription(exhibitData.description);
        let exhibitThumbnail = this.getExhibitThumbnail(exhibitData.thumbnail);
        let exhibitBody = this.getExhibitBody(exhibitData.body);

        exhibitMainContainer.appendChild(exhibitImage);
        exhibitMainContainer.appendChild(exhibitName);
        exhibitMainContainer.appendChild(exhibitSpecies);
        exhibitMainContainer.appendChild(exhibitDescription);
        exhibitMainContainer.appendChild(exhibitThumbnail);
        exhibitMainContainer.appendChild(exhibitBody);

        
        exhibit = exhibitMainContainer; //(container + content)
    
        return exhibit;
    
    },

    display : function (Location,exhibitsData) {
        let htmlLocation = document.querySelector(Location);

        for(let exhibitData of exhibitsData){
            let exhibit = this.getExhibit(exhibitData);
            htmlLocation.appendChild(exhibit);
            //console.log(book);
    }
},

};

function loadExhibitData(){
    fetch('https://gist.githubusercontent.com/Grandozzy/efd6370731039e63ef4d0485da18aeb7/raw/8e7789e5d9f898fd1385ef31c2a382a8fe7c3e79/Exhibits.json')
        .then(response => response.json())
        .then(function(exhibits){
             console.log(exhibits);
             John.ExhibitData.display('#root',exhibits);
            //console.log(window);
            //window.data = posts;
  });

}