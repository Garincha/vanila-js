"use strict";

// Fetch URL Function


//this part of code is for json() method
function loadData() {
    const url = "https://gist.githubusercontent.com/runspired/a607f4debabde043efd284a04b244974/raw/0b0a502200c12d99ffafab96f3aa07e04a126f81/twiddle.json";
    fetch(url)
        //.then((resp) => resp.text())
        .then((resp) => resp.json())
        .then(function (data) {
            //let books_data  = eval(data);// eval() method is insecured, not recommended to use. 
            //let books_data = JSON.parse(data);// this line works same like above line.
            //let books_data = data;
            //console.log(Object.keys(books_data)); by this line of code, we can see the property/key of the object/collection(array)
            //console.log(books_data);
            //John.htmlTable.display(books_data, '#root');
            John.htmlTable.display(data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});

/*
// this part of code for eval() method. 
function loadData() {
    const url = "https://gist.githubusercontent.com/runspired/a607f4debabde043efd284a04b244974/raw/0b0a502200c12d99ffafab96f3aa07e04a126f81/twiddle.json";
    fetch(url)
        .then((resp) => resp.text())
        //.then((resp) => resp.json())
        .then(function (data) {
            let books_data  = eval(data);// eval() method is insecured, not recommended to use. 
            //let books_data = JSON.parse(data);// this line works same like above line.
            //let books_data = data;
            //console.log(Object.keys(books_data)); by this line of code, we can see the property/key of the object/collection(array)
            //console.log(books_data);
            John.htmlTable.display(books_data, '#root');
            //John.htmlTable.display(data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});

// this part of code is for JSON.parse() method
function loadData() {
    const url = "https://gist.githubusercontent.com/runspired/a607f4debabde043efd284a04b244974/raw/0b0a502200c12d99ffafab96f3aa07e04a126f81/twiddle.json";
    
    fetch(url)
        .then((resp) => resp.text())
        //.then((resp) => resp.json())
        .then(function (data) {
            //let books_data  = eval(data);// eval() method is insecured, not recommended to use. 
            let books_data = JSON.parse(data);// this line works same like above line.
            //let books_data = data;
            //console.log(Object.keys(books_data)); by this line of code, we can see the property/key of the object/collection(array)
            //console.log(books_data);
            John.htmlTable.display(books_data, '#root');
            //John.htmlTable.display(data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});

*/

