import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  name: attr(),
  parent: belongsTo('person', { async: false, inverse: 'children' }),
  children: hasMany('person', { async: false, inverse: 'parent' })
});
