import Ember from 'ember';

export default Ember.Controller.extend({
  appName: 'Ember Twiddle',
  
  actions: {
    createParent(name) {
      if (!name) { return }
      let store = this.get('store');
      let person = store.createRecord('person', { name });
      this.set('parent', person);
      this.set('newPersonName', '');
    },
    
    createChild(name) {
      if (!name) { return }
      let store = this.get('store');
      let parent = this.get('parent');
      let person = store.createRecord('person', { name, parent });
      this.set('newPersonName', '');
    },
    
    save() {
      this.get('parent').save({
        adapterOptions: {
          transactionMembers: ['children']
        }
      });
    }
  }
});
