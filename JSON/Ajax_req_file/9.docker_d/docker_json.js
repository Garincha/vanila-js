"use strict";

const John = {};

John.DockerData = {

    getDockerDataId : function (id) {
        
        let dockerDataId = null;

        let dockerDataIdContainer = document.createElement('li');

        let dockerDataIdText = document.createTextNode(id);

        dockerDataIdContainer.appendChild(dockerDataIdText);

        dockerDataId = dockerDataIdContainer;
        console.log(dockerDataId);
        return dockerDataId;
    },

    getDockerDataTitle : function (title) {
        
        let dockerDataTitle = null;

        let dockerDataTitleContainer = document.createElement('li');

        let dockerDataTitleText = document.createTextNode(title);

        dockerDataTitleContainer.appendChild(dockerDataTitleText);

        dockerDataTitle = dockerDataTitleContainer;
        console.log(dockerDataTitle);
        return dockerDataTitle;
    },

    getDockerDataStyle : function (style) {
        
        let dockerDataStyle = null;

        let dockerDataStyleContainer = document.createElement('li');

        let dockerDataStyleText = document.createTextNode(style);

        dockerDataStyleContainer.appendChild(dockerDataStyleText);

        dockerDataStyle = parseJsonLastNameContainer;
        console.log(dockerDataStyle);
        return dockerDataStyle;
    },

    getDockerDataTimeZone : function (timeZone) {
        
        let dockerDataTimeZone = null;

        let dockerDataTimeZoneContainer = document.createElement('li');

        let dockerDataTimeZoneText = document.createTextNode(timeZone);

        dockerDataTimeZoneContainer.appendChild(dockerDataTimeZoneText);

        dockerDataTimeZone = dockerDataTimeZoneContainer;
        console.log(dockerDataTimeZone);
        return dockerDataTimeZone;
    },

    getDockerDataEditable : function (edit) {
        
        let dockerDataEditable = null;

        let dockerDataEditableContainer = document.createElement('li');

        let dockerDataEditableText = document.createTextNode(edit);

        dockerDataEditableContainer.appendChild(dockerDataEditableText);

        dockerDataEditable = dockerDataEditableContainer;
        console.log(dockerDataEditable);
        return dockerDataEditable;
    },

    getDockerDataSharedCrossHair : function (crossHair) {
        
        let dockerDataSharedCrossHair = null;

        let dockerDataSharedCrossHairContainer = document.createElement('li');

        let dockerDataSharedCrossHairText = document.createTextNode(crossHair);

        dockerDataSharedCrossHairContainer.appendChild(dockerDataSharedCrossHairText);

        dockerDataSharedCrossHair = parseJsonGenderContainer;
        console.log(dockerDataSharedCrossHair);
        return dockerDataSharedCrossHair;
    },

    getDockerDataHideControls : function (hideControl) {
        
        let dockerDataHideControls = null;

        let dockerDataHideControlsContainer = document.createElement('li');

        let dockerDataHideControlsText = document.createTextNode(hideControl);

        dockerDataHideControlsContainer.appendChild(dockerDataHideControlsText);

        dockerDataHideControls = dockerDataHideControlsContainer;
        console.log(dockerDataHideControls);
        return dockerDataHideControlss;
    },

    getDockerDataBody : function () {
    
        let dockerDataBody = null;
    
        let dockerDataBodyContainer = document.createElement('div');
    
        dockerDataBody = dockerDataBodyContainer;
    
        return dockerDataBody;
    },

    getDockerData : function(docData){
    
        let dockerData = null;
    
        let dockerDataMainContainer = document.createElement('div');
        //console.log();
        let dockerDataId = this.getDockerDataId(docData.id);
        let dockerDataTitle = this.getDockerDataTitle(docData.title);
        let dockerDataStyle = this.getDockerDataStyle(docData.style);
        let dockerDataTimeZone = this.getDockerDataTimeZone(docData.timezone);
        let dockerDataEditable = this.getDockerDataEditable(docData.editable);
        let dockerDataSharedCrossHair = this.getDockerDataSharedCrossHair(docData.sharedCrosshair);
        let dockerDataHideControls  = this.getDockerDataHideControls(docData.hideControls);
        let dockerDataBody  = this.getDockerDataBody(docData.body);
        dockerDataMainContainer.appendChild(dockerDataId);
        dockerDataMainContainer.appendChild(dockerDataTitle);
        dockerDataMainContainer.appendChild(dockerDataStyle);
        dockerDataMainContainer.appendChild(dockerDataTimeZone);
        dockerDataMainContainer.appendChild(dockerDataEditable);
        dockerDataMainContainer.appendChild(dockerDataSharedCrossHair);
        dockerDataMainContainer.appendChild(dockerDataHideControls);
        dockerDataMainContainer.appendChild(dockerDataBody);
        
        dockerData = dockerDataMainContainer; //(container + content)
    
        return dockerData;
    
    },

    display : function (Location,dockersData) {
        let htmlLocation = document.querySelector(Location);

        for(let dockerData of dockersData){
            let docker = this.getParseJson(dockerData);
            htmlLocation.appendChild(docker);
            //console.log(book);
    }
},

};

function loadDockerData(){
    fetch('https://gist.githubusercontent.com/billimek/c6d2cf56cc5330710ed2b48a8f736a4d/raw/5bc1e31c1a390aafa8d63191f30c53af62aa1bcb/docker.json')
        .then(response => response.json())
        .then(function(dockers){
             console.log(dockers);
             John.DockerData.display('#root',dockers);
            //console.log(window);
            //window.data = posts;
  });

}