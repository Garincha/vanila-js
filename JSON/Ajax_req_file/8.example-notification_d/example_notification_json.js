"use strict";

const John = {};

John.ExampleNotificationData = {

    getExampleMessageId : function (msgId) {
        
        let exampleMessageId = null;

        let exampleMessageIdContainer = document.createElement('li');

        let exampleMessageIdText = document.createTextNode(msgId);

        exampleMessageIdContainer.appendChild(exampleMessageIdText);

        exampleMessageId = exampleMessageIdContainer;
        console.log(exampleMessageId);
        return exampleMessageId;
    },

    getExampleType : function (type) {
        
        let exampleType = null;

        let exampleTypeContainer = document.createElement('li');

        let exampleTypeText = document.createTextNode(type);

        exampleTypeContainer.appendChild(exampleTypeText);

        exampleType = exampleTypeContainer;
        console.log(exampleType);
        return exampleType;
    },

    getExampleToken : function (token) {
        
        let exampleToken = null;

        let exampleTokenContainer = document.createElement('li');

        let exampleTokenText = document.createTextNode(token);

        exampleTokenContainer.appendChild(exampleTokenText);

        exampleToken = exampleTokenContainer;
        console.log(exampleToken);
        return exampleToken;
    },

    

    getExampleTopicArn: function (topicArn) {
        
        let exampleTopicArn = null;

        let exampleTopicArnContainer = document.createElement('li');

        let exampleTopicArnText = document.createTextNode(topicArn);

        exampleTopicArnContainer.appendChild(exampleTopicArnText);

        exampleTopicArn = exampleTopicArnContainer;
        console.log(exampleTopicArn);
        return exampleTopicArn;
    },

    getExampleMessage : function (message) {
        
        let exampleMessage = null;

        let exampleMessageContainer = document.createElement('li');

        let exampleMessageText = document.createTextNode(message);

        permSearchModuleContainer.appendChild(exampleMessageText);

        exampleMessage = exampleMessageContainer;
        console.log(exampleMessage);
        return exampleMessage;
    },

    getExampleSubscribeURL : function (subscribeUrl) {
        
        let exampleSubscribeUrl = null;

        let exampleSubscribeUrlContainer = document.createElement('li');

        let exampleSubscribeUrlText = document.createTextNode(subscribeUrl);

        exampleSubscribeUrlContainer.appendChild(exampleSubscribeUrlText);

        exampleSubscribeUrl = exampleSubscribeUrlContainer;
        console.log(exampleSubscribeUrl);
        return exampleSubscribeUrl;
    },

    getExampleTimeStamp : function (timestamp) {
        
        let exampleTimeStamp = null;

        let exampleTimeStampContainer = document.createElement('li');

        let exampleTimeStampText = document.createTextNode(timestamp);

        exampleTimeStampContainer.appendChild(exampleTimeStampText);

        exampleTimeStamp = exampleTimeStampContainer;
        console.log(exampleTimeStamp);
        return exampleTimeStamp;
    },

    getExampleSignatureVersion : function (sigVersion) {
        
        let exampleSignatureVersion = null;

        let exampleSignatureVersionContainer = document.createElement('li');

        let exampleSignatureVersionText = document.createTextNode(sigVersion);

        exampleSignatureVersionContainer.appendChild(exampleSignatureVersionText);

        exampleSignatureVersion = exampleSignatureVersionContainer;
        console.log(exampleSignatureVersion);
        return exampleSignatureVersion;
    },

    getExampleSignature : function (Signature) {
        
        let exampleSignature = null;

        let exampleSignatureContainer = document.createElement('li');

        let exampleSignatureText = document.createTextNode(Signature);

        exampleSignatureContainer.appendChild(exampleSignatureText);

        exampleSignature = exampleSignatureContainer;
        console.log(exampleSignature);
        return exampleSignature;
    },

    getExampleSigningCertURL : function (SignatureUrl) {
        
        let exampleSigningCertUrl = null;

        let exampleSigningCertUrlContainer = document.createElement('li');

        let exampleSigningCertUrlText = document.createTextNode(SignatureUrl);

        exampleSigningCertUrlContainer.appendChild(exampleSigningCertUrlText);

        exampleSigningCertUrl = exampleSigningCertUrlContainer;
        console.log(exampleSigningCertUrl);
        return exampleSigningCertUrl;
    },

    getExampleBody : function () {
    
        let exampleBody = null;
    
        let exampleBodyContainer = document.createElement('div');
    
        exampleBody = exampleBodyContainer;
    
        return exampleBody;
    },

    getExample : function(exampleData){
    
        let example = null;
    
        let exampleMainContainer = document.createElement('div');
        //console.log();
        let exampleMessageId = this.getExampleMessageId(exampleData.MessageId);
        let exampleType = this.getExampleType(exampleData.Type);
        //let exampleToken = this.getExampleToken(exampleData.Token);
        let exampleTopicArn = this.getExampleTopicArn(exampleData.TopicArn);
        let exampleMessage = this.getExampleMessage(exampleData.Message);
        let exampleSubscribeURL = this.getExampleSubscribeURL(exampleData.SubscribeURL);
        let exampleTimeStamp = this.getExampleTimeStamp(exampleData.Timestamp);
        let exampleSignatureVersion = this.getExampleSignatureVersion(exampleData.SignatureVersion);
        let exampleSignature = this.getExampleSignature(exampleData.Signature);
        let exampleSigningCertURL = this.getExampleSigningCertURL(exampleData.SigningCertURL);
        let exampleBody = this.getExampleBody(exampleData.body);

        exampleMainContainer.appendChild(exampleMessageId);
        exampleMainContainer.appendChild(exampleType);
        //exampleMainContainer.appendChild(exampleToken);
        exampleMainContainer.appendChild(exampleTopicArn);
        exampleMainContainer.appendChild(exampleMessage);
        exampleMainContainer.appendChild(exampleSubscribeURL);
        exampleMainContainer.appendChild(exampleTimeStamp);
        exampleMainContainer.appendChild(exampleSignatureVersion);
        exampleMainContainer.appendChild(exampleSignature);
        exampleMainContainer.appendChild(exampleSigningCertURL);
        exampleMainContainer.appendChild(exampleBody);

        
        example = exampleMainContainer; //(container + content)
    
        return example;
    
    },

    display : function (Location,examplesData) {
        let htmlLocation = document.querySelector(Location);

        for(let exampleData of examplesData){
            let example = this.getExample(exampleData);
            htmlLocation.appendChild(example);
            //console.log(book);
    }
},

};

function loadExampleData(){
    fetch('https://gist.githubusercontent.com/swistak/8780039/raw/cb55f5f4c65fdcb7cf35e887ea3b81c3aafeb9c7/example_notification.json')
        .then(response => response.json())
        .then(function(examples){
             console.log(examples);
             John.ExampleNotificationData.display('#root',examples);
            //console.log(window);
            //window.data = posts;
  });

}