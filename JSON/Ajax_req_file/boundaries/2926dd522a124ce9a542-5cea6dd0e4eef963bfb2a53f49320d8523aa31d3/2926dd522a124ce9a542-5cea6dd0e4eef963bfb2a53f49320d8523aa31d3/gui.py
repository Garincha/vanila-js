from tkinter import ttk, messagebox, filedialog
from tkinter import *
from collections import namedtuple
from math import floor
import traceback
import json


def tryorerror(func):
    def meta(*args, **kw):
        try:
            return func(*args, **kw)
        except Exception as e:
            print(traceback.format_exc())
            messagebox.showerror(e.__class__.__name__, "Error: %s" % e)
    return meta

class Application(Frame):
    units_path = "boundaries.json"

    def __init__(self, root):
        super().__init__(root)

        self.grid()
        self.try_load_units()
        self.create_widgets()

    def load_units(self, path):
        with open(path, "r") as f:
            self.units = json.load(f)

    @tryorerror
    def try_load_units(self):
        try:
            self.load_units(self.units_path)
        except FileNotFoundError:
            fmt = "Failed to load grade boundaries. Would you like to load a different file?"
            if not messagebox.askyesno("FileNotFoundError", fmt.format(self.units_path)):
                exit(-1)

            try:
                self.load_units(filedialog.askopenfilename())
            except:
                raise ValueError("The file you selected is invalid")

    def create_widgets(self):
        self.lbl_mark = Label(self, text="Raw mark:")
        self.lbl_mark.grid(column=0, row=0)

        self.ent_mark = Entry(self)
        self.ent_mark.grid(column=1, row=0)

        self.var_unit = StringVar()

        cmb_options = ("Unit",) + tuple(self.units.keys())

        self.cmb_units = ttk.OptionMenu(self, self.var_unit, *cmb_options)
        self.cmb_units.grid(column=2, row=0)

        self.lbl_ums = Label(self, text="UMS mark:")
        self.lbl_ums.grid(column=0, row=2)

        self.ent_ums = Entry(self)
        self.ent_ums.grid(column=1, row=2)

        self.btn_calc = Button(self, text="Calculate", command=self.btn_calc_pressed)
        self.btn_calc.grid(column=2, row=2)

    def get_unit_where(self, **kwargs):
        for unit in self.units:
            for key,val in self.kwargs.items():
                if unit.get(key, None) == val:
                    return unit
        return None

    def yield_units_where(self, **kwargs):
        for unit in self.units:
            for key,val in self.kwargs.items():
                if unit.get(key, None) == val:
                    yield unit

    def 

    def raw_to_ums(self, mark, unit):
        raw = unit["raw"]
        ums = unit["ums"]

        for i,boundary in enumerate(raw):
            if mark >= boundary and mark <= raw[0]:
                break
        else:
            raise ValueError("Mark '{}' not within grade boundaries for unit {} (max mark={})"
                .format(mark, self.var_unit.get(), raw[0]))

        raw_diff = raw[i - 1] - raw[i]
        ums_diff = ums[i - 1] - ums[i]

        return floor((ums_diff / raw_diff) * (mark - raw[i])) + ums[i]

    @tryorerror
    def calculate_ums(self):
        try:
            code = self.var_unit.get()
            unit = self.get_unit_where(code=code)
        except:
            raise ValueError("Please select a unit")

        try:
            raw_mark = int(self.ent_mark.get())
        except ValueError:
            data = self.ent_mark.get()
            if not data:
                raise ValueError("Please enter a raw mark")
            raise ValueError("Invalid raw mark '{}'".format(self.ent_mark.get()))
        ums_mark = self.raw_to_ums(raw_mark, unit)

        self.ent_ums.delete(0, END)
        self.ent_ums.insert(0, str(ums_mark))

    def btn_calc_pressed(self):
        self.calculate_ums()


def main():
    root = Tk()
    root.title("Raw mark to UMS - v0.1")

    app = Application(root)

    root.mainloop()

if __name__ == "__main__":
    main()
