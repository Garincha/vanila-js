"use strict";

const John = {};

John.SampleResponse = {

    getSampleResponseId : function (id) {
        
        let sampleResponseId = null;

        let sampleResponseIdContainer = document.createElement('li');

        let sampleResponseIdText = document.createTextNode(id);

        sampleResponseIdContainer.appendChild(sampleResponseIdText);

        sampleResponseId = sampleResponseIdContainer;
        console.log(sampleResponseId);
        return sampleResponseId;
    },

    getSampleResponseTo : function (toInt) {
        
        let sampleResponseTo = null;

        let sampleResponseToContainer = document.createElement('li');

        let sampleResponseToText = document.createTextNode(toInt);

        sampleResponseToContainer.appendChild(sampleResponseToText);

        sampleResponseTo = sampleResponseToContainer;
        console.log(sampleResponseTo);
        return sampleResponseTo;
    },

    getSampleResponseText : function (text) {
        
        let sampleResponseText = null;

        let sampleResponseTextContainer = document.createElement('li');

        let sampleResponseTextTitle = document.createTextNode(text);

        sampleResponseTextContainer.appendChild(sampleResponseTextTitle);

        sampleResponseText = sampleResponseTextContainer;
        console.log(sampleResponseText);
        return sampleResponseText;
    },

    getSampleResponseSubject : function (subject) {
        
        let sampleResponsubject = null;

        let sampleResponsubjectContainer = document.createElement('li');

        let sampleResponsubjectText = document.createTextNode(subject);

        sampleResponsubjectContainer.appendChild(sampleResponsubjectText);

        sampleResponsubject = sampleResponsubjectContainer;
        console.log(sampleResponsubject);
        return sampleResponsubject;
    },

    getSampleResponseReceived : function (receive) {
        
        let sampleResponseReceive = null;

        let sampleResponseReceiveContainer = document.createElement('li');

        let sampleResponsubjectText = document.createTextNode(receive);

        sampleResponseReceiveContainer.appendChild(sampleResponsubjectText);

        sampleResponseReceive = sampleResponseReceiveContainer;
        console.log(sampleResponseReceive);
        return sampleResponseReceive;
    },

    getSampleResponseRawMime : function (rawmime) {
        
        let sampleResponseRaw_mime = null;

        let sampleResponseRaw_mimeContainer = document.createElement('li');

        let sampleResponseRaw_mimeText = document.createTextNode(rawmime);

        sampleResponseRaw_mimeContainer.appendChild(sampleResponseRaw_mimeText);

        sampleResponseRaw_mime = sampleResponseRaw_mimeContainer;
        console.log(sampleResponseRaw_mime);
        return sampleResponseRaw_mime;
    }, 
    
    getSampleResponseOriginalhtml : function (OrgHtml) {
        
        let SampleResponseOriginalhtml = null;

        let SampleResponseOriginalhtmlContainer = document.createElement('li');

        let SampleResponseOriginalhtmlText = document.createTextNode(OrgHtml);

        SampleResponseOriginalhtmlContainer.appendChild(SampleResponseOriginalhtmlText);

        SampleResponseOriginalhtml = SampleResponseOriginalhtmlContainer;
        console.log(SampleResponseOriginalhtml);
        return SampleResponseOriginalhtml;
    }, 

    getSampleResponseHtml : function (rawmime) {
        
        let SampleResponseHtml = null;

        let SampleResponseHtmlContainer = document.createElement('li');

        let SampleResponseHtmlText = document.createTextNode(rawmime);

        SampleResponseHtmlContainer.appendChild(SampleResponseHtmlText);

        SampleResponseHtml = SampleResponseHtmlContainer;
        console.log(SampleResponseHtml);
        return SampleResponseHtml;
    },

    getSampleResponseBody : function () {
    
        let SampleResponseBody = null;
    
        let SampleResponseBodyContainer = document.createElement('div');
    
        SampleResponseBody = SampleResponseBodyContainer;
    
        return SampleResponseBody;
    },

    getSampleResponse : function(sampleData){
    
        let sampleResponse = null;
    
        let sampleResponseMainContainer = document.createElement('div');
        //console.log();
        let sampleResponseId = this.getSampleResponseId(sampleData.id);
        let sampleResponseTo = this.getSampleResponseTo(sampleData.to);
        let sampleResponseText = this.getSampleResponseText(sampleData.text);
        let sampleResponseSubject = this.getSampleResponseSubject(sampleData.subject);
        let sampleResponseReceived = this.getSampleResponseReceived(sampleData.received);
        let sampleResponseRawMime = this.getSampleResponseRawMime(sampleData.raw_mime);
        let sampleResponseOriginalhtml = this.getSampleResponseOriginalhtml(sampleData.original_html);
        let sampleResponseHtml = this.getSampleResponseHtml(sampleData.html);
        let sampleResponseBody = this.getSampleResponseBody(sampleData.body);
        sampleResponseMainContainer.appendChild(sampleResponseId);
        sampleResponseMainContainer.appendChild(sampleResponseTo);
        sampleResponseMainContainer.appendChild(sampleResponseText);
        sampleResponseMainContainer.appendChild(sampleResponseSubject);
        sampleResponseMainContainer.appendChild(sampleResponseReceived);
        sampleResponseMainContainer.appendChild(sampleResponseRawMime);
        sampleResponseMainContainer.appendChild(sampleResponseOriginalhtml);
        sampleResponseMainContainer.appendChild(sampleResponseHtml);
        sampleResponseMainContainer.appendChild(sampleResponseBody);
     
    
        sampleResponse = sampleResponseMainContainer; //(container + content)
    
        return sampleResponse;
    
    },

    display : function (Location,samplesData) {
        let htmlLocation = document.querySelector(Location);

        for(let sampleData of samplesData){
            let sample = this.getSampleResponse(sampleData);
            htmlLocation.appendChild(sample);
            //console.log(book);
    }
},
    
};

function loadsamples(){
    fetch('https://gist.githubusercontent.com/jamesduncombe/515eaeba7b76ee14c7e6bdb58c69b766/raw/01c9d8774e34049f3044849fe96903c45e55f6ff/sample%2520response.json')
        .then(response => response.json())
        .then(function(samples){
             console.log(samples);
             John.SampleResponse.display('#root',samples);
            //console.log(window);
            //window.data = posts;
  });

}