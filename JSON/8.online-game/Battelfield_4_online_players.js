
{

    "pc": {
        "label": "PC",
        "count": 9286,
        "peak24": 9653
    },
    "ps3": {
        "label": "PS3",
        "count": 3280,
        "peak24": 3800
    },
    "xbox": {
        "label": "XBOX360",
        "count": 367,
        "peak24": 507
    },
    "xone": {
        "label": "XBOXONE",
        "count": 3943,
        "peak24": 6149
    },
    "ps4": {
        "label": "PS4",
        "count": 8549,
        "peak24": 8857
    }

}
