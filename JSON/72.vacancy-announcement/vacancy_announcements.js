
{

    "metadata": {
        "responseInfo": {
            "status": 200,
            "developerMessage": "OK"
        },
        "resultset": {
            "count": 2772,
            "pagesize": 2,
            "page": 0
        },
        "executionTime": 0.10628199577331543
    },
    "results": [
        {
            "about_office": "<p>The USAO for the District of Alaska is charged with the primary responsibility of representing the United States in court. The Criminal Division is responsible for prosecuting all federal crimes ranging from acts of terrorism to public corruption, white-collar crime, gang and gun crimes, internet-related crimes, and environmental crimes. Through its Civil Division, the Office is charged with defending agencies of the United States, enforcing regulatory agencies� authority, and recovering funds from violators of U.S. criminal, regulatory, and civil laws.  An internship with U.S. Attorney�s Office for the District of Alaska offers a unique and challenging experience to work in criminal prosecution or civil litigation in the Last Frontier.  Assignments include, but are not limited to, drafting responsive motions, dispositive motions, memoranda of law, and appellate briefs. Interns become familiar with the rules of evidence and either the Federal Rules of Civil or Criminal Procedure depending on placement with Criminal or Civil Division. In addition, interns may attend depositions, meetings with agents, and accompany the attorneys to observe court proceedings.  By local rule, law students in their second semester of their second year of law school, or law students in their third-year of law school, have a unique opportunity to be approved to appear in court at hearings and participate in trials.</p>\n",
            "application_process": "<p>Cover letter, resume, transcript, e-mail address and telephone number where student can be reached both day and evening, two references with phone numbers and email address and preference for Anchorage, Fairbanks, or Juneau and preference for Criminal or Civil Division.  Also include any specific interest in working in Alaska.</p>\n\n<p><a href=\"/usao/ak/\">USAO District of Alaska</a></p>\n\n<p>Telephone:  (907) 271-5071<br />\nFax:  (907) 271-1500<br />\nEmail: Aunnie Steward at <a href=\"mailto:USAAK.Intern.Applicatoins@usdoj.gov\">USAAK.Intern.Applications@usdoj.gov</a> <br />\nApplications may be sent via email to the above email address.</p>\n\n<p><b>Deadline:  Applications for summer internships may be submitted beginning in September preceding the summer for which application is made. The deadline for summer internship applications is February 1 of the year for which application is made.  Positions will be filled on a rolling basis.  All decisions will be made by March 1.  If interested in an internship for fall, winter or spring, please provide an application at least six weeks prior to desired start date.</b></p>\n",
            "body": "<p>Perform research projects and assist in discovery, motion practice, and trial preparation. To ensure that all interns finish the summer with a good writing sample, every intern will work under the guidance of an AUSA to prepare documents to be filed with the court. Interns will be given assignments with as many different attorneys as possible, providing exposure to a wider variety of practice areas and professional contacts. We also have brown bag lunches at which AUSAs, the defense bar, the judiciary, and federal agents speak. Our goal is to introduce our interns to the federal legal system, to develop their legal skills and, above all, to enjoy their summer here in the Last Frontier (with 20+ hours of daylight).</p>\n\n<p>Internship Locations:   Anchorage (4); Fairbanks (1); Juneau (1)</p>\n\n<p> </p>\n",
            "changed": "1541699734",
            "created": "1399914723",
            "deadline": null,
            "hiring_office": null,
            "hiring_org": {
                "uuid": "089db8e3-1ca9-43f8-9bd1-f608b3e927a0",
                "name": "USAO District of Alaska"
            },
            "job_id": null,
            "language": "en",
            "location": {
                "country": "US",
                "administrative_area": "AK",
                "locality": "Anchorage",
                "postal_code": "99513",
                "thoroughfare": "222 West 7th Avenue, #9",
                "sub_premise": null,
                "phone_number": "",
                "phone_number_extension": "",
                "mobile_number": "",
                "fax_number": ""
            },
            "num_positions": "6",
            "position": "law_student_volunteer_academic_year",
            "practice_area": "oarm-civilliti",
            "qualifications": "<p>Law students that have completed their first year. Law school graduates are not eligible for student positions. Students chosen will be subject to a background investigation, which can take up to three months. Must be a U.S. citizen. Must provide information for a background investigation (includes inquiry into, suitability issues such as illegal activity such as drug use, outstanding debts, tax information, etc). Final approval for all applicants is then obtained from the Department of Justice in Washington, D.C.</p>\n\n<p>Minimum Participation Required: 8-10 weeks</p>\n",
            "relocation_expenses": "None",
            "salary": "Work-study credit possible.",
            "title": "Law Student Volunteer, Academic Year",
            "travel": "None",
            "url": "https://www.justice.gov/legal-careers/job/usao-district-alaska",
            "uuid": "92db84e4-3e3d-41f0-bc79-c1b9d2153e35",
            "vuuid": "4c49a870-43c3-4758-b4dc-fc0d2a0112cc"
        },
        {
            "about_office": "<p>The USAO for the Central District of California provides extern opportunities to law students during the fall and spring semesters as well as the summer.  Duty is on a volunteer (non-paid) basis.</p>\n",
            "application_process": "<p>Law students wishing to apply for an externship should submit, via email to AUSA Julie Zatz at the address above, a single pdf containing, in order, a cover letter, resume, law school transcript (if available) and writing sample.  The writing sample should be 10 pages or less.</p>\n\n<p>USAO Central District of California<br />\nCivil Division<br />\nFederal Building, Suite 7211<br />\n300 N. Los Angeles Street<br />\nLos Angeles, CA 90012</p>\n\n<p>ATTN:  Julie Zatz, Assistant U.S. Attorney<br />\nTelephone:  (213) 894-7349<br />\nFax:  (213) 894-7819</p>\n\n<p>Application Deadlines:  <strong>Fall: March 15 /Spring: September 15/ Summer: December 15</strong></p>\n",
            "body": "<p>Assignments may include assisting attorneys with legal research, writing pleadings, trial preparation, and completing appellate briefs. Externs are encouraged to observe depositions, trials and other court proceedings in cases on which they have worked or in cases of general interest in the office. Externs are also encouraged to attend meetings with client agencies and to attend settlement conferences, site visits, and witness interviews.</p>\n\n<p>Internship Location:  Los Angeles</p>\n\n<p>Minimum Weeks Required:  10 � 12 (a minimum of 3 full days per week including Mondays)</p>\n\n<p>Web Sites:  <a href=\"/usao/cac/law_student_jobs.html\">www.justice.gov/usao/cac/law_student_jobs.html</a><br /><a href=\"/usao/cac/civil.html\">www.justice.gov/usao/cac/civil.html</a></p>\n",
            "changed": "1545157232",
            "created": "1399914723",
            "deadline": null,
            "hiring_office": null,
            "hiring_org": {
                "uuid": "650c20e9-9a64-4d93-b4f7-60447635156e",
                "name": "USAO Central District of California"
            },
            "job_id": null,
            "language": "en",
            "location": {
                "country": "US",
                "administrative_area": "CA",
                "locality": "Los Angeles",
                "postal_code": "90012",
                "thoroughfare": "Federal Building, Suite 7211",
                "sub_premise": null,
                "phone_number": "",
                "phone_number_extension": "",
                "mobile_number": "",
                "fax_number": ""
            },
            "num_positions": "2 - 4",
            "position": "law_student_volunteer_academic_year",
            "practice_area": "oarm-civilliti",
            "qualifications": "<p>Second- or third-year law students. Class ranking in the top 25%.  Must pass a federal background check and be a United States citizen.</p>\n",
            "relocation_expenses": null,
            "salary": "Law school credit possible.",
            "title": "Law Student Volunteer, Academic Year and Summer, Civil Division",
            "travel": "None",
            "url": "https://www.justice.gov/legal-careers/job/usao-central-district-california-civil-division-federal-building-suite-7211-300-n",
            "uuid": "d6912a82-202c-4435-ba1b-589443c720b0",
            "vuuid": "83fe76e9-2891-4bb8-a389-081f31445148"
        }
    ]

}
