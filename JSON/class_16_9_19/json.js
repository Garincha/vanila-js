const items = ['laptop','Mouse','Monitor'];

const laptop = {
    brand : 'HP',
    color : 'Silver',
    size : 15.6,
    price : 45000,
}

const products = [
    {
        type : 'Laptop',
        brand : 'HP',
        color : 'Silver',
        size : 15.6,
        price : 45000, 
    },
    {
        type : 'Mouse',
        brand : 'A4TEC',
        color : 'black',
        size : 'normal',
        price : 300, 
    },
    {
        type : 'Monitor',
        brand : 'Logitec',
        size : 14.1,
        price : 5000, 
    }

];
/*
//accessing JSON data
console.log(items);
console.log(laptop);
console.log(products);
console.log(items[2]);
console.log(laptop.color);
console.log(laptop.price);
console.log(products[2]);
console.log(products[1].price);

// reasign/modify JSON data
items[0] = 'Desktop';
console.log(items[0]);
laptop.brand = 'Lenovo';
console.log(laptop.brand);
products[2].brand = 'Digitic';
console.log(products[2].brand);

// loop

for(index in items){
    console.log(index);// getting position
    console.log(items[index]);//getting value
}

for (const item of items) { 
    console.log(items);// getting value
}

for(index2 in laptop){
    console.log(index2);//getting position
    console.log(laptop[index2]);//getting value
}
*/
for(x of products){
    console.log(x);
}