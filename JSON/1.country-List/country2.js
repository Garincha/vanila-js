"use strict";

// Fetch URL Function
function loadData() {
    const john = "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
    fetch(john)
        .then((resp) => resp.text())
        .then(function (data) {
            let users_data  = eval(data);
            console.log(users_data);
            John.htmlTable.display(users_data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});