"use strict";
const countries = [
    {
        "serial" : 1,
        "name" : `Afghanistan`,
        "code" : `AF`,
    },
    {
        "serial" : 2,
        "name" : `Åland Islands`,
        "code" : `AX`,
    },
    {
        "serial" : 3,
        "name" : `Albania`,
        "code": `AL`,
    },
    {
       "serial" : 4,
       "name" : `Algeria`,
       "code" : `DZ`,
    },
    {
        "serial" : 5,
        "name" : `American Samoa`,
        "code" : `AS`,
    },
    {
        "serial" : 6,
        "name" : `AndorrA`,
        "code" : `AD`,
    },
    {
        "serial" : 7,
        "name" : `Angola`,
        "code" : `AO`,
    },
    {
        "serial" : 8,
        "name" : `Anguilla`,
        "code" : `AI`,
    },
    {
        "serial" : 9,
        "name" : `Antarctica`,
        "code" : `AQ`,
    },
    {
        "serial" : 10,
        "name" : `Antigua and Barbuda`,
        "code" : `AG`,
    },
    {
        "serial" : 11,
        "name" : `Argentina`,
        "code" : `AR`,
    },
    {
        "serial" : 12,
        "name" : `Armenia`,
        "code" : `AM`,
    },
    {
        "serial" : 13,
        "name" : `Aruba`,
        "code" : `AW`,
    },
    {
        "serial" : 14,
        "name" : `Australia`,
        "code" : `AU`,
    },
    {
        "serial" : 15,
        "name" : `Austria`,
        "code" : `AT`,
    },
    {
        "serial" : 16,
        "name" : `Azerbaijan`,
        "code" : `AZ`,
    },
    {
        "serial" : 17,
        "name" : `Bahamas`,
        "code" : `BS`,
    },
    {
        "serial" : 18,
        "name" : `Bahrain`,
        "code": `BH`,
    },
    {
        "serial" : 19,
        "name" : `Bangladesh`,
        "code" : `BD`,
    },
    {
        "serial" : 20,
        "name" : `Barbados`,
        "code" : `BB`,
    },
    {
        "serial" : 21,
        "name" : `Belarus`,
        "code" : `BY`,
    },
    {
        "serial" : 22,
        "name" : `Belgium`,
        "code" : `BE`,
    },
    {
        "serial" : 23,
        "name" : `Belize`,
        "code" : `BZ`,
    },
    {
        "serial" : 24,
        "name" : `Benin`,
        "code" : `BJ`,
    },
    {
        "serial" : 25,
        "name" : `Bermuda`,
        "code" : `BM`,
    },
    {
        "serial" : 26,
        "name" : `Bhutan`,
        "code" : `BT`,
    },
    {
        "serial" : 27,
        "name" : `Bolivia`,
        "code" : `BO`,
    },
    {
        "serial" : 28,
        "name" : `Bosnia and Herzegovina`,
        "code" : `BA`,
    },
    {
        "serial" : 29,
        "name" : `Botswana`,
        "code" : `BW`,
    },
    {
        "serial" : 30,
        "name" : `Bouvet Island`,
        "code" : `BV`,
    },
    {
        "serial" : 31,
        "name" : `Brazil`,
        "code" : `BR`,
    },
    {
        "serial" : 32,
        "name" : `British Indian Ocean Territory`,
        "code" : `IO`,
    },
    {
        "serial" :33,
        "name" : `Brunei Darussalam`,
        "code": `BN`,
    },
    {
        "serial" : 34,
        "name" : `Bulgaria`,
        "code" : `BG`,
    },
    {
        "serial" : 35,
        "name" : `Burkina Faso`,
        "code" : `BF`,
    },
    {
        "serial" : 36,
        "name" : `Burundi`,
        "code" : `BI`,
    },
    {
        "serial" : 37,
        "name" : `Cambodia`,
        "code" : `KH`,
    },
    {
        "serial" : 38,
        "name" : `Cameroon`,
        "code" : `CM`,
    },
    {
        "serial" : 39,
        "name" : `Canada`,
        "code" : `CA`,
    },
    {
        "serial" : 40,
        "name" : `Cape Verde`,
        "code" : `CV`,
    },
    {
        "serial" : 41,
        "name" : `Cayman Islands`,
        "code" : `KY`,
    },
    {
        "serial" : 42,
        "name" : `Central African Republic`,
        "code" : `CF`,
    },
    {
        "serial" : 43,
        "name" : `Chad`,
        "code" : `TD`,
    },
    {
        "serial" : 44,
        "name" : `Chile`,
        "code" : `CL`,
    },
    {
        "serial" : 45,
        "name" : `China`,
        "code" : `CN`,
    },
    {
        "serial" : 46,
        "name" : `Christmas Island`,
        "code" : `CX`,
    },
    {
        "serial" : 47,
        "name" : `Cocos (Keeling) Islands`,
        "code" : `CC`,
    },
    {
        "serial" : 48,
        "name" : `Colombia`,
        "code" : `CO`,
    },
    {
        "serial" : 49,
        "name" : `Comoros`,
        "code" : `KM`,
    },
    {
        "serial" : 50,
        "name" : `Congo`,
        "code" : `CG`,
    },
    {
        "serial" : 51,
        "name" : `Congo, The Democratic Republic of the`,
        "code": `CD`,
    },
    {
        "serial" : 52,
        "name" : `Cook Islands`,
        "code" : `CK`,
    },
    {
        "serial" : 53,
        "name" : `Costa Rica`,
        "code" : `CR`,
    },
    {
        "serial" : 54,
        "name" : `Cote D\'Ivoire`,
        "code" : `CI`,
    },
    {
        "serial" : 55,
        "name" : `Croatia`,
        "code" : `HR`,
    },
    {
        "serial" : 56,
        "name" : `Cuba`,
        "code" : `CU`,
    },
    {
        "serial" : 57,
        "name" : `Cyprus`,
        "code" : `CY`,
    },
    {
        "serial" : 58,
        "name" : `Czech Republic`,
        "code" : `CZ`,
    },

    {
        "serial" : 59,
        "name" : `Denmark`,
        "code": `DK`,
    },
    {
        "serial" : 60,
        "name" : `Djibouti`,
        "code" : `DJ`,
    },
    {
        "serial" : 61,
        "name" : `Dominica`,
        "code" : `DM`,
    },
    {
        "serial" : 62,
        "name" : `Dominican Republic`,
        "code" : `DO`,
    },
    {
        "serial" : 63,
        "name" : `Ecuador`,
        "code" : `EC`,
    },
    {
        "serial" : 64,
        "name" : `Egypt`,
        "code" : `EG`,
    },
    {
        "serial" : 65,
        "name" : `El Salvador`,
        "code" : `SV`,
    },
    {
        "serial" : 66,
        "name" : `Equatorial Guinea`,
        "code" : `GQ`,
    },
    {
        "serial" : 67,
        "name" : `Eritrea`,
        "code" : `ER`,
    },
    {
        "serial" : 68,
        "name" : `Estonia`,
        "code" : `EE`,
    },
    {
        "serial" : 69,
        "name" : `Ethiopia`,
        "code" : `ET`,
    },
    {
        "serial" : 70,
        "name" : `Falkland Islands (Malvinas)`,
        "code" : `FK`,
    },
    {
        "serial" : 71,
        "name" : `Faroe Islands`,
        "code" : `FO`,
    },
    {
        "serial" : 72,
        "name" : `Fiji`,
        "code" : `FJ`,
    },
    {
        "serial" : 73,
        "name" : `Finland`,
        "code": `FI`,
    },
    {
        "serial" : 74,
        "name" : `France`,
        "code" : `FR`,
    },
    {
        "serial" : 75,
        "name" : `French Guiana`,
        "code" : `GF`,
    },
    {
        "serial" : 76,
        "name" : `French Polynesia`,
        "code" : `PF`,
    },
    {
        "serial" : 77,
        "name" : `French Southern Territories`,
        "code" : `TF`,
    },
    {
        "serial" : 78,
        "name" : `Gabon`,
        "code" : `GA`,
    },
    {
        "serial" : 79,
        "name" : `Gambia`,
        "code" : `GM`,
    },
    {
        "serial" : 80,
        "name": `Georgia`,
        "code" : `GE`,
    },
    {
        "serial" : 81,
        "name" : `Germany`,
        "code" : `DE`,
    },
    {
        "serial" : 82,
        "name" : `Ghana`,
        "code" : `GH`,
    },
    {
        "serial" : 83,
        "name" : `Gibraltar`,
        "code" : `GI`,
    },
    {
        "serial" : 84,
        "name" : `Greece`,
        "code" : `GR`,
    },
    {
        "serial" : 85,
        "name" : `Greenland`,
        "code" : `GL`,
    },
    {
        "serial" : 86,
        "name" : `Grenada`,
        "code" : `GD`,
    },
    {
        "serial" : 87,
        "name" : `Guadeloupe`,
        "code" : `GP`,
    },
    {
        "serial" : 88,
        "name" : `Guam`,
        "code" : `GU`,
    },
    {
        "serial" : 89,
        "name" : `Guatemala`,
        "code": `GT`,
    },
    {
        "serial" : 90,
        "name" : `Guernsey`,
        "code" : `GG`,
    },
    {
        "serial" : 91,
        "name": `Guinea`,
        "code" : `GN`,
    },
    {
        "serial" : 92,
        "name" : `Guinea-Bissau`,
        "code" : `GW`,
    },
    {
        "serial" : 93,
        "name" : `Guyana`,
        "code" : `GY`,
    },
    {
        "serial" : 94,
        "name" : `Haiti`,
        "code": `HT`,
    },
    {
        "serial" : 95,
        "name" : `Heard Island and Mcdonald Islands`,
        "code" : `HM`,
    },
    {
        "serial" : 96,
        "name" : `Holy See (Vatican City State)`,
        "code" : `VA`,
    },
    {
        "serial" : 97,
        "name" : `Honduras`,
        "code" : `HN`,
    },
    {
        "serial" : 98,
        "name" : `Hong Kong`,
        "code" : `HK`,
    },
    {
        "serial" : 99,
        "name" : `Hungary`,
        "code" : `HU`,
    },
    {
        "serial" : 100,
        "name": `Iceland`,
        "code" : `IS`,
    },
    {
        "serial" : 101,
        "name": `India`,
        code : `IN`,
    },
    {
        "serial" : 102,
        "name" : `Indonesia`,
        "code" : `ID`,
    },
    {
        "serial" : 103,
        "name" : `Iran, Islamic Republic Of`,
        "code" : `IR`,
    },
    {
        "serial" : 104,
        "name" : `Iraq`,
        "code" : `IQ`,
    },
    {
        "serial" : 105,
        "name" : `Ireland`,
        "code" : `IE`,
    },
    {
        "serial" : 106,
        "name" : `Isle of Man`,
        "code" : `IM`,
    },
    {
        "serial" : 107,
        "name" : `Israel`,
        "code": `IL`,
    },
    {
        "serial" : 108,
        "name" : `Italy`,
        "code" : `IT`,
    },
    {
        "serial" : 109,
        "name" : `Jamaica`,
        "code" : `JM`,
    },
    {
        "serial" : 110,
        "name" : `Japan`,
        "code" : `JP`,
    },
    {
        "serial" : 111,
        "name" : `Jersey`,
        "code" : `JE`,
    },
    {
        "serial" : 112,
        "name" : `Jordan`,
        "code" : `JO`,
    },
    {
        "serial" : 113,
        "name" : `Kazakhstan`,
        "code" : `KZ`,
    },
    {
        "serial" : 114,
        "name" : `Kenya`,
        "code" : `KE`,
    },
    {
        "serial" : 115,
        "name" : `Kiribati`,
        "code" : `KI`,
    },
    {
        "serial" : 116,
        "name" : `Korea, Democratic People\'S Republic of`,
        "code" : `KP`,
    },
    {
        "serial" : 117,
        "name" : `Korea, Republic of`,
        "code" : `KR`,
    },
    {
        "serial" : 118,
        "name" : `Kuwait`,
        "code" : `KW`,
    },
    {
        "serial" : 119,
        "name" : `Kyrgyzstan`,
        "code" : `KG`,
    },
    {
        "serial" : 120,
        "name" : `Lao People\'S Democratic Republic`,
        "code" : `LA`,
    },
    {
        "serial" : 121,
        "name" : `Latvia`,
        "code" : `LV`,
    },
    {
        "serial" : 122,
        "name" : `Lebanon`,
        "code": `LB`,
    },
    {
        "serial" : 123,
        "name" : `Lesotho`,
        "code" : `LS`,
    },
    {
        "serial" : 124,
        "name" : `Liberia`,
        "code" : `LR`,
    },
    {
        "serial" : 125,
        "name" : `Libyan Arab Jamahiriya`,
        "code" : `LY`,
    },
    {
        "serial" : 126,
        "name" : `Liechtenstein`,
        "code" : `LI`,
    },
    {
        "serial" : 127,
        "name" : `Lithuania`,
        "code" : `LT`,
    },
    {
        "serial" : 128,
        "name" : `Luxembourg`,
        "code" : `LU`,
    },
    {
        "serial" : 129,
        "name" : `Macao`,
        "code" : `MO`,
    },
    {
        "serial" : 130,
        "name" : `Macedonia, The Former Yugoslav Republic of`,
        "code": `MK`,
    },
    {
        "serial" : 131,
        "name" : `Madagascar`,
        "code" : `MG`,
    },
    {
        "serial" : 132,
        "name" : `Malawi`,
        "code": `MW`,
    },
    {
        "serial" : 133,
        "name" : `Malaysia`,
        "code" : `MY`,
    },
    {
        "serial" : 134,
        "name" : `Maldives`,
        "code" : `MV`,
    },
    {
        "serial" : 135,
        "name" : `Mali`,
        "code" : `ML`,
    },

    {
        "serial" : 136,
        "name" : `Malta`,
        "code" : `MT`,
    },
    {
        "serial" : 137,
        "name": `Marshall Islands`,
        "code" : `MH`,
    },
    {
        "serial" : 138,
        "name" : `Martinique`,
        "code": `MQ`,
    },
    {
        "serial" : 139,
        "name" : `Mauritania`,
        "code" : `MR`,
    },
    {
        "serial" : 140,
        "name" : `Mauritius`,
        "code" : `MU`,
    },
    {
        "serial" : 141,
        "name" : `Mayotte`,
        "code" : `YT`,
    },
    {
        "serial" : 142,
        "name" : `Mexico`,
        "code" : `MX`,
    },
    {
        "serial" : 143,
        "name" : `Micronesia, Federated States of`,
        "code" : `FM`,
    },
    {
        "serial" : 144,
        "name" : `Moldova, Republic of`,
        "code" : `MD`,
    },
    {
        "serial" : 145,
        "name" : `Monaco`,
        "code" : `MC`,
    },
    {
        "serial" : 146,
        "name" : `Mongolia`,
        "code" : `MN`,
    },
    {
        "serial" : 147,
        "name" : `Montserrat`,
        "code" : `MS`,
    },
    {
        "serial" : 148,
        "name" : `Morocco`,
        "code" : `MA`,
    },
    {
        "serial" : 149,
        "name" : `Mozambique`,
        "code" : `MZ`,
    },
    {
        "serial" : 150,
        "name" : `Myanmar`,
        "code" : `MM`,
    },
    {
        "serial" : 151,
        "name": `Namibia`,
        "code" : `NA`,
    },
    {
        "serial" : 152,
        "name" : `Nauru`,
        "code" : `NR`,
    },
    {
        "serial" : 153,
        "name" : `Nepal`,
        "code" : `NP`,
    },
    {
        "serial" : 154,
        "name" : `Netherlands`,
        "code" : `NL`,
    },
    {
        "serial" : 155,
        "name" : `Netherlands Antilles`,
        "code" : `AN`,
    },
    {
        "serial" : 156,
        "name" : `New Caledonia`,
        "code" : `NC`,
    },
    {
        "serial" : 157,
        "name" : `New Zealand`,
        "code" : `NZ`,
    },
    {
        "serial" : 158,
        "name" : `Nicaragua`,
        "code" : `NI`,
    },
    {
        "serial" : 159,
        "name" : `Niger`,
        "code" : `NE`,
    },
    {
        "serial" : 160,
        "name" : `Nigeria`,
        "code" : `NG`,
    },
    {
        "serial" : 161,
        "name" : `Niue`,
        "code" : `NU`,
    },
    {
        "serial" : 162,
        "name": `Norfolk Island`,
        "code" : `NF`,
    },
    {
        "serial" : 163,
        "name" : `Northern Mariana Islands`,
        "code" : `MP`,
    },
    {
        "serial" : 164,
        "name" : `Norway`,
        "code" : `NO`,
    },
    {
        "serial" : 165,
        "name" : `Oman`,
        "code": `OM`,
    },
    {
        "serial" : 166,
        "name" : `Pakistan`,
        "code" : `PK`,
    },
    {
        "serial" : 167,
        "name" : `Palau`,
        "code": `PW`,
    },
    {
        "serial" : 168,
        "name" : `Palestinian Territory, Occupied`,
        "code": `PS`,
    },
    {
        "serial" : 169,
        "name" : `Panama`,
        "code" : `PA`,
    },
    {
        "serial" : 170,
        "name" : `Papua New Guinea`,
        "code" : `PG`,
    },
    {
        "serial" : 171,
        "name" : `Paraguay`,
        "code" : `PY`,
    },
    {
        "serial" : 172,
        "name" : `Peru`,
        "code" : `PE`,
    },
    {
        "serial" : 173,
        "name": `Philippines`,
        "code": `PH`,
    },
    {
        "serial" : 174,
        "name" : `Pitcairn`,
        "code" : `PN`,
    },
    {
        "serial" : 175,
        "name" : `Poland`,
        "code" : `PL`,
    },
    {
        "serial" : 176,
        "name" : `Portugal`,
        "code" : `PT`,
    },
    {
        "serial" : 177,
        "name" : `Puerto Rico`,
        "code" : `PR`,
    },
    {
        "serial" : 178,
        "name" : `Qatar`,
        "code" : `QA`,
    },
    {
        "serial" : 179,
        "name" : `Reunion`,
        "code": `RE`,
    },
    {
        "serial" : 180,
        "name" : `Romania`,
        "code" : `RO`,
    },
    {
        "serial" : 181,
        "name": `Russian Federation`,
        "code" : `RU`,
    },
    {
        "serial" : 182,
        "name" : `RWANDA`,
        "code" : `RW`,
    },
    {
        "serial" : 183,
        "name" : `Saint Helena`,
        "code": `SH`,
    },
    {
        "serial" : 184,
        "name" : `Saint Kitts and Nevis`,
        "code" : `KN`,
    },
    {
        "serial" : 185,
        "name" : `Saint Lucia`,
        "code" : `LC`,
    },
    {
        "serial" : 186,
        "name" : `Saint Pierre and Miquelon`,
        "code" : `PM`,
    },
    {
        "serial" : 187,
        "name" : `Saint Vincent and the Grenadines`,
        "code" : `VC`,
    },
    {
        "serial" : 188,
        "name" : `Samoa`,
        "code" : `WS`,
    },
    {
        "serial" : 189,
        "name" : `San Marino`,
        "code" : `SM`,
    },
    {
        "serial" : 190,
        "name" : `Sao Tome and Principe`,
        "code" : `ST`,
    },
    {
        "serial" : 191,
        "name" : `Saudi Arabia`,
        "code" : `SA`,
    },
    {
        "serial" : 192,
        "name" : `Senegal`,
        "code" : `SN`,
    },
    {
        "serial" : 193,
        "name" : `Serbia and Montenegro`,
        "code" : `CS`,
    },
    {
        "serial" : 194,
        "name" : `Seychelles`,
        "code" : `SC`,
    },
    {
        "serial" : 195,
        "name" : `Sierra Leone`,
        "code" : `SL`,
    },
    {
        "serial" : 196,
        "name" : `Singapore`,
        "code" : `SG`,
    },
    {
        "serial" : 197,
        "name" : `Slovakia`,
        "code": `SK`,
    },
    {
        "serial" : 198,
        "name" : `Slovenia`,
        "code" : `SI`,
    },
    {
        "serial" : 199,
        "name" : `Solomon Islands`,
        "code" : `SB`,
    },
    {
        "serial" : 200,
        "name": `Somalia`,
        "code" : `SO`,
    },
    {
        "serial" : 201,
        "name" : `South Africa`,
        "code" : `ZA`,
    },
    {
        "serial" : 202,
        "name" : `South Georgia and the South Sandwich Islands`,
        "code" : `GS`,
    },
    {
        "serial" : 203,
        "name" : `Spain`,
        "code" : `ES`,
    },
    {
        "serial" : 204,
        "name": `Sri Lanka`,
        "code" : `LK`,
    },
    {
        "serial" : 205,
        "name" : `Sudan`,
        "code" : `SD`,
    },
    {
        "serial" : 206,
        "name" : `Suriname`,
        "code" : `SR`,
    },
    {
        "serial" : 207,
        "name" : `Svalbard and Jan Mayen`,
        "code" : `SJ`,
    },
    {
        "serial" : 208,
        "name" : `Swaziland`,
        "code" : `SZ`,
    },
    {
        "serial" : 209,
        "name" : `Sweden`,
        "code" : `SE`,
    },
    {
        "serial" : 210,
        "name" : `Switzerland`,
        "code" : `CH`,
    },
    {
        "serial" : 211,
        "name" : `Syrian Arab Republic`,
        "code" : `SY`,
    },
    {
        "serial" : 212,
        "name" : `Taiwan, Province of China`,
        "code": `TW`,
    },
    {
        "serial" : 213,
        "name" : `Tajikistan`,
        "code" : `TJ`,
    },
    {
        "serial" : 214,
        "name" : `Tanzania, United Republic of`,
        "code" : `TZ`,
    },
    {
        "serial" : 215,
        "name" : `Thailand`,
        "code" : `TH`,
    },
    {
        "serial" : 216,
        "name" : `Timor-Leste`,
        "code" : `TL`,
    },
    {
        "serial" : 217,
        "name" : `Togo`,
        "code" : `TG`,
    },
    {
        "serial" : 218,
        "name" : `Tokelau`,
        "code" : `TK`,
    },
    {
        "serial" : 219,
        "name" : `Tonga`,
        "code" : `TO`,
    },
    {
        "serial" : 220,
        "name": `Trinidad and Tobago`,
        "code" : `TT`,
    },
    {
        "serial" : 221,
        "name" : `Tunisia`,
        "code" : `TN`,
    },
    {
        "serial" : 222,
        "name" : `Turkey`,
        "code" : `TR`,
    },
    {
        "serial" : 223,
        "name": `Turkmenistan`,
        "code" : `TM`,
    },
    {
        "serial" : 224,
        "name" : `Turks and Caicos Islands`,
        "code" : `TC`,
    },
    {
        "serial" : 225,
        "name" : `Tuvalu`,
        "code" : `TV`,
    },
    {
        "serial" : 226,
        "name" : `Uganda`,
        "code" : `UG`,
    },
    {
        "serial" : 227,
        "name" : `Ukraine`,
        "code" : `UA`,
    },
    {
        "serial" : 228,
        "name": `United Arab Emirates`,
        "code" : `AE`,
    },
    {
        "serial" : 229,
        "name" : `United Kingdom`,
        "code" : `GB`,
    },
    {
        "serial" : 230,
        "name" : `United States`,
        "code": `US`,
    },
    {
        "serial" : 231,
        "name": `United States Minor Outlying Islands`,
        "code" : `UM`,
    },
    {
        "serial" : 232,
        "name" : `Uruguay`,
        "code" : `UY`,
    },
    {
        "serial" : 233,
        "name" : `Uzbekistan`,
        "code" : `AF`,
    },
    {
        "serial" : 234,
        "name" : `Vanuatu`,
        "code" : `VU`,
    },
    {
        "serial" : 235,
        "name" : `Venezuela`,
        "code" : `VE`,
    },
    {
        "serial" : 236,
        "name" : `Viet Nam`,
        "code" : `VN`,
    },
    {
        "serial" : 237,
        "name" : `Virgin Islands, British`,
        "code" : `VG`,
    },
    {
        "serial" : 238,
        "name" : `Virgin Islands, U.S.`,
        "code" : `VI`,
    },
    {
        "serial" : 239,
        "name": `Wallis and Futuna`,
        "code" : `WF`,
    },
    {
        "serial" : 240,
        "name" : `Western Sahara`,
        "code" : `EH`,
    },
    {
        "serial" : 241,
        "name" : `Western Sahara`,
        "code" : `EH`,
    },
    {
        "serial" : 242,
        "name" : `Yemen`,
        "code" : `YE`,
    },
    {
        "serial" : 243,
        "name" : `Zambia`,
        "code" : `ZM`,
    },
    {
        "serial" : 244,
        "name" : `Zimbabwe`,
        "code" : `ZW`,
    },
    
];

const table = document.getElementById('country_list');

for(const country of countries){

    let tr = "";

    tr += `<tr>`;
    tr += `<td>`+country.serial+`</td>`;
    tr += `<td>`+country.name+`</td>`;
    tr += `<td>`+country.code+`</td>`;
    tr += '</tr>';

    table.innerHTML += tr;
    console.log(tr);
}