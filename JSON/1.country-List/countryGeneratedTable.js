'use strict';

// Generate Table by Js Code:


const John = {};
John.htmlTable = {
    buildTh: function (column) {
        let theadText = document.createTextNode(column[0].toUpperCase() + column.slice(1));
        let th = document.createElement("th");
        th.appendChild(theadText);
        return th;
    },
    buildTableHead: function (titles) {
        let tr = document.createElement("tr");
        for (let title of titles) {
            let th = this.buildTh(title);
            tr.appendChild(th);
        }
        return tr;
    },
    buildTd: function (column) {
        let text = document.createTextNode(column);
        let td = document.createElement("td");
        td.appendChild(text);
        return td;
    },

    buildTr: function (objRowData) {
        let tr = document.createElement("tr");

        for (let proparty in objRowData) {

            let td = this.buildTd(objRowData[proparty]);
            tr.appendChild(td);

        }

        return tr;
    },

    buildTableBody: function (collection) {
        let tbody = document.createElement('tbody');
        for (let data of collection) {

            let tr = this.buildTr(data);
            tbody.appendChild(tr);

        }

        return tbody;
    },
    buildTable: function (collection) {
        let table = document.createElement("table");
        let titles = Object.keys(collection[0]);
        let tHead = this.buildTableHead(titles);
        let tBody = this.buildTableBody(collection);
        table.appendChild(tHead);
        table.appendChild(tBody);
        // table.border = 1; // trivial way
        table.setAttribute('border', '1'); // Nodes discussion

        return table;
    },
    display: function (data, domlocation) {
        let container = document.querySelector(domlocation);
        let table = this.buildTable(data);
        container.appendChild(table);

        return true;
    }
};