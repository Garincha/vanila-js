/*
category : Database
type : Description of multiple entity
*/

"use strict";

const data = (function () {
  const database = [
    {
    "id": "5968dd23fc13ae04d9000001",
    "product_name": "sildenafil citrate",
    "supplier": "Wisozk Inc",
    "quantity": 261,
    "unit_cost": "$10.47",
    "shipment_charge" : "$0.75"
  }, 
  {
    "id": "5968dd23fc13ae04d9000002",
    "product_name": "Mountain Juniperus ashei",
    "supplier": "Keebler-Hilpert",
    "quantity": 292,
    "unit_cost": "$8.74",
    "shipment_charge" : "$0.35"
  }, 
  {
    "id": "5968dd23fc13ae04d9000003",
    "product_name": "Dextromathorphan HBr",
    "supplier": "Schmitt-Weissnat",
    "quantity": 211,
    "unit_cost": "$20.53",
    "shipment_charge" : "$0.55"
  },
  {
    "id": "5968dd23fc13ae04d9000004",
    "product_name": "Dextromathorphan HBC",
    "supplier": "Schmitt-Weissnat",
    "quantity": 322,
    "unit_cost": "$30.39",
    "shipment_charge" : "$0.25"
  },
  {
    "id": "5968dd23fc13ae04d9000005",
    "product_name": "Mountain ashei",
    "supplier": "Keebler-Hilpert",
    "quantity": 142,
    "unit_cost": "$6.97",
    "shipment_charge" : "$0.95"
  }, 

  ];

return database;
})();//This is IIFE - Imediately Invoke Function Expression. This function gets ready & execute itself(the code inside it) simultaneously, always execute onetime. 

function createTableHead(headerText) {
  let headContent = document.createTextNode(headerText);
  let  head = document.createElement('TH');
  head.appendChild(headContent);

  return head;
}

function createTableCell(cellText) {
  let cellContent = document.createTextNode(cellText);
  let cell = document.createElement('TD');
  cell.appendChild(cellContent);

  return cell;
}

 //creating table for data
let table = document.createElement('table');

// creating a row
let tr = document.createElement('tr');

//function to create table head dynamically
for(let property in data[0]){// by data[0], assigning the key's value of data[0]->(id,product_name,supplier,quantity,unit_cost,shipment_charge) inside properyy. By this loop, we're getting the table head dynimically. 
    tr.appendChild(createTableHead(property.toUpperCase()));
}

table.appendChild(tr);

//Table rows 

for(let product of data){// by this function, we're making various node(through JS) to make a table

    let td = '';
    let tr = document.createElement('tr');

    for(let property in product){// this is a inner loop, the benefit of this loop is we can randomly add a total object as a data or a single property inside a object. getting all data from a json dynamically. 
        let td = createTableCell(product[property]);// here,by property we're passing the value of id,product_name,supplier,quantity,unit_cost,shipment_charge through this inner loop. 
        tr.appendChild(td);
        
    }

    table.appendChild(tr);
    
}

let container = document.querySelector("#root");
container.appendChild(table);
  