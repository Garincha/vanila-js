/*
category : Database
type : Description of multiple entity
*/

"use strict";

const data = (function () {
  const database = [
    {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000001"
    },
    "product_name": "sildenafil citrate",
    "supplier": "Wisozk Inc",
    "quantity": 261,
    "unit_cost": "$10.47"
  }, 
  {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000002"
    },
    "product_name": "Mountain Juniperus ashei",
    "supplier": "Keebler-Hilpert",
    "quantity": 292,
    "unit_cost": "$8.74"
  }, 
  {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000003"
    },
    "product_name": "Dextromathorphan HBr",
    "supplier": "Schmitt-Weissnat",
    "quantity": 211,
    "unit_cost": "$20.53"
  }

  ];

return database;
})();//This is IIFE - Imediately Invoke Function Expression. This function gets ready & execute itself(the code inside it) simultaneously, always execute onetime. 

function createTableHead(headerText) {
    let headContent = document.createTextNode(headerText);
    let  head = document.createElement('TH');
    head.appendChild(headContent);

    return head;
}

function createTableCell(cellText) {
    let cellContent = document.createTextNode(cellText);
    let cell = document.createElement('TD');
    cell.appendChild(cellContent);

    return cell;
}

let table = document.createElement('table'); //creating table (Virtual Dom)
let tr = document.createElement('tr');// creating a row
tr.appendChild(createTableHead('Id'));// creating a table head
tr.appendChild(createTableHead('Product Name'));
tr.appendChild(createTableHead('Supplier'));
tr.appendChild(createTableHead('Quantity'));
tr.appendChild(createTableHead('Unit Cost'));

table.appendChild(tr);

for(let product of data){// by this function, we're making various node(through JS) to make a table

    let cellId = createTableCell(product._id.$oid);
    let cellProductName = createTableCell(product.product_name);
    let cellSupplier = createTableCell(product.supplier);
    let cellQuantity = createTableCell(product.quantity);
    let cellUnitCost = createTableCell(product.unit_cost);

    let tr = document.createElement('tr');

    tr.appendChild(cellId);
    tr.appendChild(cellProductName);
    tr.appendChild(cellSupplier);
    tr.appendChild(cellQuantity);
    tr.appendChild(cellUnitCost);

    table.appendChild(tr);

}

console.log(table);

let container = document.querySelector("#root");
container.appendChild(table);
  