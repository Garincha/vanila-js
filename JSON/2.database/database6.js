/*
category : Database
type : Description of multiple entity
*/

"use strict";

const data = (function () {
  const database = [{
      "id": "5968dd23fc13ae04d9000001",
      "product_name": "sildenafil citrate",
      "supplier": "Wisozk Inc",
      "quantity": 261,
      "unit_cost": "$10.47",
      "shipment_charge": "$0.75"
    },
    {
      "id": "5968dd23fc13ae04d9000002",
      "product_name": "Mountain Juniperus ashei",
      "supplier": "Keebler-Hilpert",
      "quantity": 292,
      "unit_cost": "$8.74",
      "shipment_charge": "$0.35"
    },
    {
      "id": "5968dd23fc13ae04d9000003",
      "product_name": "Dextromathorphan HBr",
      "supplier": "Schmitt-Weissnat",
      "quantity": 211,
      "unit_cost": "$20.53",
      "shipment_charge": "$0.55"
    },
    {
      "id": "5968dd23fc13ae04d9000004",
      "product_name": "Dextromathorphan HBC",
      "supplier": "Schmitt-Weissnat",
      "quantity": 322,
      "unit_cost": "$30.39",
      "shipment_charge": "$0.25"
    },
    {
      "id": "5968dd23fc13ae04d9000005",
      "product_name": "Mountain ashei",
      "supplier": "Keebler-Hilpert",
      "quantity": 142,
      "unit_cost": "$6.97",
      "shipment_charge": "$0.95"
    }

  ];

  return database;
})(); //This is IIFE - Imediately Invoke Function Expression. 
console.log(data);

const John = {};

John.HtmlTable = {

    buildTh : function (column) {
        let theadText = document.createTextNode(column.toUpperCase());
        let th = document.createElement("th");
        th.appendChild(theadText);
        return th;
    },

    buildTableHead : function(titles){
        let tr = document.createElement("tr");

        for(let title of titles){
              let th = John.HtmlTable.buildTh(title);
              tr.appendChild(th);  
        }

        return tr;
    },

    buildTbody: function(column) {
        let tdCell = document.createTextNode(column);
        let td = document.createElement("td");
        td.appendChild(tdCell);
        return td;
      },

      buildTableBody: function(collection) {
 
        let tbody = document.createElement('TBODY');
        for (let data of collection) {
          let tr = document.createElement("tr");
          let td = "";
          for (let property in data) {
    
            td = John.HtmlTable.buildTbody(data[property]);
            tr.appendChild(td);
         
          }
         
          tbody.appendChild(tr);
          
        }
       
        return tbody;
      },

    buildTable : function (collection) {
       let table = document.createElement('table'); 
       let titles = Object.keys(collection[0]);// collecting table heads from data(here collection is data)
       let tHead = John.HtmlTable.buildTableHead(titles);// making table heads
       let tBody = John.HtmlTable.buildTableBody(collection);
       table.appendChild(tHead);
       table.appendChild(tBody);
       // table.border = 1; // trivial way
       table.setAttribute('border', '1'); // Nodes discussion
    
       return table;
    },

    display : function (data,domLocation) {
       let container = document.querySelector(domLocation); 
       let table = John.HtmlTable.buildTable(data);
       container.appendChild(table);
    }
};

John.HtmlTable.display(data,'#root');