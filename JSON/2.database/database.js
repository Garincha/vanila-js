/*
category : Database
type : Description of multiple entity
*/

"use strict";

const database = [
    {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000001"
    },
    "product_name": "sildenafil citrate",
    "supplier": "Wisozk Inc",
    "quantity": 261,
    "unit_cost": "$10.47"
  }, 
  {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000002"
    },
    "product_name": "Mountain Juniperus ashei",
    "supplier": "Keebler-Hilpert",
    "quantity": 292,
    "unit_cost": "$8.74"
  }, 
  {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000003"
    },
    "product_name": "Dextromathorphan HBr",
    "supplier": "Schmitt-Weissnat",
    "quantity": 211,
    "unit_cost": "$20.53"
  }
];

console.log(database[0]._id.$oid);

for(let key in database){
    //console.log(key);// getting position
    //console.log(database[key]);//getting value

    const container = document.getElementById('json-database-info');
    container.innerHTML += "<li>"+ key +"</li>";
    //container.innerHTML += "<li>"+(database[key]) +"</li>";
}

for (const value of database) { 

    const container2 = document.getElementById('json-database-show');
    container2.innerHTML += "<li>"+ value +"</li>";// not showing the value inside object through html output
    //console.log(value);// getting value
}

const table = document.getElementById('product_list');

for(const product of database){
   let tr = "";

   tr += "<tr>";
   tr += "<td>"+product._id.$oid +"</td>";
   tr += "<td>"+product.product_name +"</td>";
   tr += "<td>"+product.supplier +"</td>";
   tr += "<td>"+product.quantity +"</td>";
   tr += "<td>"+product.unit_cost +"</td>";

   table.innerHTML += tr;
   console.log(tr);
}
  