/*
category : Database
type : Description of multiple entity
*/

"use strict";

const data = (function () {
  const database = [
    {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000001"
    },
    "product_name": "sildenafil citrate",
    "supplier": "Wisozk Inc",
    "quantity": 261,
    "unit_cost": "$10.47"
  }, 
  {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000002"
    },
    "product_name": "Mountain Juniperus ashei",
    "supplier": "Keebler-Hilpert",
    "quantity": 292,
    "unit_cost": "$8.74"
  }, 
  {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000003"
    },
    "product_name": "Dextromathorphan HBr",
    "supplier": "Schmitt-Weissnat",
    "quantity": 211,
    "unit_cost": "$20.53"
  }

  ];

return database;
})();//This is IIFE - Imediately Invoke Function Expression. This function gets ready & execute itself(the code inside it) simultaneously, always execute onetime. 


const table = document.createElement('table'); //Virtual Dom

for(const product of data){// by this function, we're making various node(through JS) to make a table

    let itemIdText = document.createTextNode(product._id.$oid);
    let itemIdTd = document.createElement('td');
    itemIdTd.appendChild(itemIdText);

    let itemProductNameText = document.createTextNode(product.product_name);
    let itemProductNameTd = document.createElement('td');
    itemProductNameTd.appendChild(itemProductNameText);

    let itemSupplierText = document.createTextNode(product.supplier);
    let itemSupplierTd = document.createElement('td');
    itemSupplierTd.appendChild(itemSupplierText);

    let itemQuantityText = document.createTextNode(product.quantity);
    let itemQuantityTd = document.createElement('td');
    itemQuantityTd.appendChild(itemQuantityText);

    let itemUnitCostText = document.createTextNode(product.unit_cost);
    let itemUnitCostTd = document.createElement('td');
    itemUnitCostTd.appendChild(itemUnitCostText);

    let tr = document.createElement('tr');
    tr.appendChild(itemIdTd);
    tr.appendChild(itemProductNameTd);
    tr.appendChild(itemSupplierTd);
    tr.appendChild(itemQuantityTd);
    tr.appendChild(itemUnitCostTd);

    table.appendChild(tr);

}

console.log(table);

let container = document.querySelector('#root');
container.appendChild(table);
  