
{

    "type": "FeatureCollection",
    "metadata": {
        "generated": 1568618473000,
        "url": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_hour.geojson",
        "title": "USGS All Earthquakes, Past Hour",
        "status": 200,
        "api": "1.8.1",
        "count": 14
    },
    "features": [
        {
            "type": "Feature",
            "properties": {
                "mag": 1.7,
                "place": "56km NNW of Valdez, Alaska",
                "time": 1568618290161,
                "updated": 1568618467145,
                "tz": -540,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ak019bwggsrw",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ak019bwggsrw.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 44,
                "net": "ak",
                "code": "019bwggsrw",
                "ids": ",ak019bwggsrw,",
                "sources": ",ak,",
                "types": ",geoserve,origin,",
                "nst": null,
                "dmin": null,
                "rms": 0.68999999999999995,
                "gap": null,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.7 - 56km NNW of Valdez, Alaska"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -146.6901,
                    61.607500000000002,
                    20.600000000000001
                ]
            },
            "id": "ak019bwggsrw"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.3,
                "place": "18km NNW of Anchorage, Alaska",
                "time": 1568617637927,
                "updated": 1568617794776,
                "tz": -540,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ak019bwgegj4",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ak019bwgegj4.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 26,
                "net": "ak",
                "code": "019bwgegj4",
                "ids": ",ak019bwgegj4,",
                "sources": ",ak,",
                "types": ",geoserve,origin,",
                "nst": null,
                "dmin": null,
                "rms": 0.32000000000000001,
                "gap": null,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.3 - 18km NNW of Anchorage, Alaska"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -150.08539999999999,
                    61.3566,
                    39.600000000000001
                ]
            },
            "id": "ak019bwgegj4"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.9199999999999999,
                "place": "12km SE of Volcano, Hawaii",
                "time": 1568617415760,
                "updated": 1568617758350,
                "tz": -600,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/hv71145567",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/hv71145567.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 57,
                "net": "hv",
                "code": "71145567",
                "ids": ",hv71145567,",
                "sources": ",hv,",
                "types": ",geoserve,origin,phase-data,",
                "nst": 46,
                "dmin": 0.040989999999999999,
                "rms": 0.20999999999999999,
                "gap": 87,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.9 - 12km SE of Volcano, Hawaii"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -155.16099550000001,
                    19.3390007,
                    3.7400000000000002
                ]
            },
            "id": "hv71145567"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 0.33000000000000002,
                "place": "9km NE of Aguanga, CA",
                "time": 1568617178330,
                "updated": 1568617438883,
                "tz": -480,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ci38839207",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ci38839207.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 2,
                "net": "ci",
                "code": "38839207",
                "ids": ",ci38839207,",
                "sources": ",ci,",
                "types": ",geoserve,nearby-cities,origin,phase-data,scitech-link,",
                "nst": 18,
                "dmin": 0.015100000000000001,
                "rms": 0.13,
                "gap": 60,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 0.3 - 9km NE of Aguanga, CA"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -116.7915,
                    33.499166700000004,
                    6.1500000000000004
                ]
            },
            "id": "ci38839207"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.5900000000000001,
                "place": "13km NNW of Pinnacles, CA",
                "time": 1568617163290,
                "updated": 1568618163235,
                "tz": -480,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/nc73274505",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/nc73274505.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 39,
                "net": "nc",
                "code": "73274505",
                "ids": ",nc73274505,",
                "sources": ",nc,",
                "types": ",geoserve,nearby-cities,origin,phase-data,scitech-link,",
                "nst": 7,
                "dmin": 0.066199999999999995,
                "rms": 0.040000000000000001,
                "gap": 214,
                "magType": "md",
                "type": "earthquake",
                "title": "M 1.6 - 13km NNW of Pinnacles, CA"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -121.2226639,
                    36.630832699999999,
                    6.0700000000000003
                ]
            },
            "id": "nc73274505"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 0.90000000000000002,
                "place": "11km SE of Alamo, Nevada",
                "time": 1568616868790,
                "updated": 1568617067258,
                "tz": -480,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/nn00704119",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/nn00704119.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 12,
                "net": "nn",
                "code": "00704119",
                "ids": ",nn00704119,",
                "sources": ",nn,",
                "types": ",geoserve,origin,phase-data,",
                "nst": 6,
                "dmin": 0.11600000000000001,
                "rms": 0.16,
                "gap": 213.16,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 0.9 - 11km SE of Alamo, Nevada"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -115.07170000000001,
                    37.296700000000001,
                    0.90000000000000002
                ]
            },
            "id": "nn00704119"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.6799999999999999,
                "place": "15km SSW of Searles Valley, CA",
                "time": 1568616793560,
                "updated": 1568617045304,
                "tz": -480,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ci38839199",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ci38839199.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 43,
                "net": "ci",
                "code": "38839199",
                "ids": ",ci38839199,",
                "sources": ",ci,",
                "types": ",geoserve,nearby-cities,origin,phase-data,scitech-link,",
                "nst": 22,
                "dmin": 0.039510000000000003,
                "rms": 0.23000000000000001,
                "gap": 57,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.7 - 15km SSW of Searles Valley, CA"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -117.4635,
                    35.642499999999998,
                    2
                ]
            },
            "id": "ci38839199"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.8999999999999999,
                "place": "38km NNE of North Nenana, Alaska",
                "time": 1568616134797,
                "updated": 1568616851315,
                "tz": -540,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ak019bwg0ir1",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ak019bwg0ir1.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 56,
                "net": "ak",
                "code": "019bwg0ir1",
                "ids": ",ak019bwg0ir1,",
                "sources": ",ak,",
                "types": ",geoserve,origin,",
                "nst": null,
                "dmin": null,
                "rms": 0.73999999999999999,
                "gap": null,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.9 - 38km NNE of North Nenana, Alaska"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -148.8366,
                    64.904799999999994,
                    17.199999999999999
                ]
            },
            "id": "ak019bwg0ir1"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.3,
                "place": "128km NW of Talkeetna, Alaska",
                "time": 1568615769107,
                "updated": 1568615985565,
                "tz": -540,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ak019bwfz80e",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ak019bwfz80e.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 26,
                "net": "ak",
                "code": "019bwfz80e",
                "ids": ",ak019bwfz80e,",
                "sources": ",ak,",
                "types": ",geoserve,origin,",
                "nst": null,
                "dmin": null,
                "rms": 0.60999999999999999,
                "gap": null,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.3 - 128km NW of Talkeetna, Alaska"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -151.75370000000001,
                    63.201000000000001,
                    9.5
                ]
            },
            "id": "ak019bwfz80e"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 2.0099999999999998,
                "place": "9km ENE of Coso Junction, CA",
                "time": 1568615664720,
                "updated": 1568616321660,
                "tz": -480,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ci38839175",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ci38839175.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 62,
                "net": "ci",
                "code": "38839175",
                "ids": ",ci38839175,",
                "sources": ",ci,",
                "types": ",focal-mechanism,geoserve,nearby-cities,origin,phase-data,scitech-link,",
                "nst": 21,
                "dmin": 0.055030000000000003,
                "rms": 0.17999999999999999,
                "gap": 93,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 2.0 - 9km ENE of Coso Junction, CA"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -117.8516667,
                    36.0625,
                    1.52
                ]
            },
            "id": "ci38839175"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.6000000000000001,
                "place": "31km ESE of Sterling, Alaska",
                "time": 1568615609682,
                "updated": 1568616176586,
                "tz": -540,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ak019bwfymg8",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ak019bwfymg8.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 39,
                "net": "ak",
                "code": "019bwfymg8",
                "ids": ",ak019bwfymg8,",
                "sources": ",ak,",
                "types": ",geoserve,origin,",
                "nst": null,
                "dmin": null,
                "rms": 0.48999999999999999,
                "gap": null,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.6 - 31km ESE of Sterling, Alaska"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -150.2166,
                    60.470300000000002,
                    39
                ]
            },
            "id": "ak019bwfymg8"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 4.5999999999999996,
                "place": "247km SE of Lambasa, Fiji",
                "time": 1568615595943,
                "updated": 1568617440040,
                "tz": -720,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/us70005gwz",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/us70005gwz.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "reviewed",
                "tsunami": 0,
                "sig": 326,
                "net": "us",
                "code": "70005gwz",
                "ids": ",us70005gwz,",
                "sources": ",us,",
                "types": ",geoserve,origin,phase-data,",
                "nst": null,
                "dmin": 3.1200000000000001,
                "rms": 0.97999999999999998,
                "gap": 55,
                "magType": "mb",
                "type": "earthquake",
                "title": "M 4.6 - 247km SE of Lambasa, Fiji"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -178.67529999999999,
                    -17.646100000000001,
                    540.25999999999999
                ]
            },
            "id": "us70005gwz"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.45,
                "place": "27km WSW of Maricopa, CA",
                "time": 1568615204310,
                "updated": 1568615446146,
                "tz": -480,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ci38839159",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ci38839159.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 32,
                "net": "ci",
                "code": "38839159",
                "ids": ",ci38839159,",
                "sources": ",ci,",
                "types": ",geoserve,nearby-cities,origin,phase-data,scitech-link,",
                "nst": 15,
                "dmin": 0.012449999999999999,
                "rms": 0.34000000000000002,
                "gap": 104,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.5 - 27km WSW of Maricopa, CA"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -119.6598333,
                    34.938166699999996,
                    11.960000000000001
                ]
            },
            "id": "ci38839159"
        },
        {
            "type": "Feature",
            "properties": {
                "mag": 1.01,
                "place": "14km ENE of Ridgecrest, CA",
                "time": 1568614885820,
                "updated": 1568615111931,
                "tz": -480,
                "url": "https://earthquake.usgs.gov/earthquakes/eventpage/ci38839151",
                "detail": "https://earthquake.usgs.gov/earthquakes/feed/v1.0/detail/ci38839151.geojson",
                "felt": null,
                "cdi": null,
                "mmi": null,
                "alert": null,
                "status": "automatic",
                "tsunami": 0,
                "sig": 16,
                "net": "ci",
                "code": "38839151",
                "ids": ",ci38839151,",
                "sources": ",ci,",
                "types": ",geoserve,nearby-cities,origin,phase-data,scitech-link,",
                "nst": 19,
                "dmin": 0.048230000000000002,
                "rms": 0.19,
                "gap": 98,
                "magType": "ml",
                "type": "earthquake",
                "title": "M 1.0 - 14km ENE of Ridgecrest, CA"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -117.5355,
                    35.661833299999998,
                    7.1900000000000004
                ]
            },
            "id": "ci38839151"
        }
    ],
    "bbox": [
        -178.6753,
        -17.6461,
        0.9,
        -115.0717,
        64.9048,
        540.26
    ]

}
