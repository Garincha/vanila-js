const items = ['laptop','Mouse','Monitor'];

const laptop = {
    "brand" : 'HP',
    "color" : 'Silver',
    "size" : 15.6,
    "price" : 45000,
}

const products = [
    {
        "id" : 1,
        "type" : 'Laptop',
        "brand" : 'HP',
        "price" : 45000, 
    },
    {
        "id" : 2,
        "type" : 'Mouse',
        "brand" : 'A4TEC',
        "price" : 300, 
    },
    {
        "id" : 3,
        "type" : 'Monitor',
        "brand" : 'Logitec',
        "price" : 5000, 
    }

]

for(index in laptop){
    //console.log(index);//getting position
    //console.log(laptop[index]);//getting value
    /*const container = document.getElementById('json_data_list');
    container.innerHTML += "<li>"+index+"</li>";// HW : use template literal{`-> this sign is called backtic} instead of "" or '';
    container.innerHTML += '<li>'+index+'</li>';
    container.innerHTML += `<li>`+index+`</li>`;
    */
   

}

for(let index in laptop){
    const laptopDetails = "<li>"+index+":"+laptop[index]+"</li>";// getting position(by "<li>"+index+"</li>") & value (laptop[index])
    const container = document.getElementById('json_data_list');
    container.innerHTML += laptopDetails;
}

for(let product in products){
    console.log(product);//getting index of product array
}

for(let product in products){
    console.log(products[product].id);// getting value of a specific index(here it is id)
}

for (const product of products) {
    console.log(product.id);// getting index & value(both) of product array
}

const table = document.getElementById('product_list');

for(const product of products){

    let tr = "";

    tr += `<tr>`;
    tr += `<td>`+product.id+`</td>`;
    tr += `<td>`+product.type+`</td>`;
    tr += `<td>`+product.brand+`</td>`;
    tr += `<td>`+product.price+`</td>`;
    tr += '</tr>';

    table.innerHTML += tr;
    console.log(tr);
}


