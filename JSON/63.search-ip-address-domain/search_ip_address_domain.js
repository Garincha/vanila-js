{
    "query": "119.30.32.121",
    "status": "success",
    "continent": "Asia",
    "continentCode": "AS",
    "country": "Bangladesh",
    "countryCode": "BD",
    "region": "C",
    "regionName": "Dhaka Division",
    "city": "Dhaka",
    "district": "",
    "zip": "1000",
    "lat": 23.7257,
    "lon": 90.4085,
    "timezone": "Asia/Dhaka",
    "currency": "BDT",
    "isp": "Grameen Phone",
    "org": "grameenphone limited",
    "as": "AS24389 grameenphone limited",
    "asname": "GRAMEENPHONE-AS-AP",
    "mobile": true,
    "proxy": false
}

{
    "dns": {
        "geo": "Bangladesh - GrameenPhone",
        "ip": "123.108.240.186"
    }
}

{
    "mtu": 1440,
    "link_type": "generic tunnel or VPN",
    "os_name": "Windows (7/8)",
    "http_name": "Firefox (60.x or newer)",
    "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"
}