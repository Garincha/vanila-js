var myVar;

function myStartFunction() {
  myVar = setInterval(function(){ alertFunc("First parameter", "Second parameter"); }, 2000);
}

function alertFunc(param1, param2) {
  document.getElementById("demo").innerHTML += "Hello ";

  document.getElementById("demo2").innerHTML = "Parameters passed to alertFunc(): <br>" 
  + param1 + "<br>" + param2 + "<br>";
}

function myStopFunction() {
  clearInterval(myVar);
}