let myVar = setInterval(myTimer, 1000);

function myTimer() {
  var date = new Date();
  var time = date.toLocaleTimeString();
  document.getElementById("demo").innerHTML = time;
}

function myStopFunction() {
  clearInterval(myVar);
}