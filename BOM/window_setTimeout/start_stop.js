let count = 0;
let time;
let timer_is_on = 0;

function timedCount() {
  document.getElementById("txt").value = count;
  count = count + 1;
  time = setTimeout(timedCount, 1000);
}

function startCount() {
  if (!timer_is_on) {
    timer_is_on = 1;
    timedCount();
  }
}

function stopCount() {
  clearTimeout(time);
  timer_is_on = 0;
}