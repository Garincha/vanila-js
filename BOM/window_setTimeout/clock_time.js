function startTime() {
    let today = new Date();
    let hour = today.getHours();
    let minute = today.getMinutes();
    let second = today.getSeconds();
    // add a zero in front of numbers<10
    minute = checkTime(minute);
    second = checkTime(second);
    document.getElementById("txt").innerHTML = hour + ":" + minute + ":" + second;
    let time = setTimeout(function(){ startTime()}, 500);
  }
  
  function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }