"use strict";

const data = (function () {
    const products = [{
            id: 1,
            date: "2015-09-01T00:00:00",
            title: "European North Sea Energy Alliance",
            sponsor: "Christina McKelvie MSP"
        },
        {
            id: 2,
            date: "2015-09-01T00:00:00",
            title: "AS it is",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 3,
            date: "2015-09-01T00:00:00",
            title: "Building Capacity, Investing in Stroke Research",
            sponsor: "Dennis Robertson MSP"
        },
        {
            id: 4,
            date: "2015-09-02T00:00:00",
            title: "Celebrating Scotland's Small and Rural Towns",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 5,
            date: "2015-09-02T00:00:00",
            title: "50 years of supporting carers",
            sponsor: "Graeme Dey MSP"
        }
    ];

    return products;
})();

function createTableHead(headerText) {

    let headContent = document.createTextNode(headerText);
    let head = document.createElement("TH");
    head.appendChild(headContent);

    return head;
}

function createTableCell(cellText) {
    
    let textNode = document.createTextNode(cellText);
    let container = document.createElement('TD');
    container.appendChild(textNode);

    return container;
}

// preparing the table for the data
// table

let table = document.createElement("table");

//thead
let tr = document.createElement('tr');
for (let property in data[0]) {
    tr.appendChild(createTableHead(property.toUpperCase()));
}
table.appendChild(tr);
// table rows
for (let product of data) {
    let td = '';
    let tr = document.createElement('tr'); // find the reason why

    for (let property in product) {
        td = createTableCell(product[property]);
        tr.appendChild(td);

    }
    table.appendChild(tr);
}
let container = document.querySelector("#root");
container.appendChild(table);