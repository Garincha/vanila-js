"use strict";

const products = [
        {
            id: 1,
            date: "2015-09-01T00:00:00",
            title: "European North Sea Energy Alliance",
            sponsor: "Christina McKelvie MSP"
        },
        {
            id: 2,
            date: "2015-09-01T00:00:00",
            title: "AS it is",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 3,
            date: "2015-09-01T00:00:00",
            title: "Building Capacity, Investing in Stroke Research",
            sponsor: "Dennis Robertson MSP"
        },
        {
            id: 4,
            date: "2015-09-02T00:00:00",
            title: "Celebrating Scotland's Small and Rural Towns",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 5,
            date: "2015-09-02T00:00:00",
            title: "50 years of supporting carers",
            sponsor: "Graeme Dey MSP"
        }
    ];

const John = {};



John.Htmltable = {
    buildTH : function () {
        
    },

    buildTbody : function () {
        
    },

    build: function (data) {
        const th = '';
        const tbody = '';
        console.log(data);
    },
    display : function (data,container) {// all value's of product assigned here in argument-> data
        const table = John.Htmltable.build(data);// creating a table
        document.getElementById(container).appendChild(table);
        
    }
};

John.Htmltable.display(products,"root");//calling the function(which is located inside display). passing products as a parameter, products is connected with data. root is an div in html page, connecting root with container 
/*
const john = {
    Htmltable = {}; we can write the above code by this way
}
*/
/*
let display = function(){
    console.log("hello"); this code is writted in other way(syntax changed) inside john.Htmltable = {} object. 
}
*/