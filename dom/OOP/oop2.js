"use strict";

let data = (function () {
    const products = [
        {
            id: 1,
            date: "2015-09-01T00:00:00",
            title: "European North Sea Energy Alliance",
            sponsor: "Christina McKelvie MSP"
        },
        {
            id: 2,
            date: "2015-09-01T00:00:00",
            title: "AS it is",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 3,
            date: "2015-09-01T00:00:00",
            title: "Building Capacity, Investing in Stroke Research",
            sponsor: "Dennis Robertson MSP"
        },
        {
            id: 4,
            date: "2015-09-02T00:00:00",
            title: "Celebrating Scotland's Small and Rural Towns",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 5,
            date: "2015-09-02T00:00:00",
            title: "50 years of supporting carers",
            sponsor: "Graeme Dey MSP"
        }
    ]; 

    return products;
})();//IIFE 

console.log(data);

const John = {};

John.HtmlTable = {

    buildTh : function (column) {
        let theadText = document.createTextNode(column.toUpperCase());
        let th = document.createElement("th");
        th.appendChild(theadText);
        return th;
    },

    buildTableHead : function(titles){
        let tr = document.createElement("tr");

        for(let title of titles){
              let th = John.HtmlTable.buildTh(title);
              tr.appendChild(th);  
        }

        return tr;
    },

    /*buildTbody: function(column) {
        let tdCell = document.createTextNode(column);
        let td = document.createElement("td");
        td.appendChild(tdCell);
        return td;
      },
    */
      buildTd: function (column) {
        let text = document.createTextNode(column);
        let td = document.createElement("td");
        td.appendChild(text);
        return td;
    },

      buildTr: function (objRowData) {
        let tr = document.createElement("tr");

        for (let proparty in objRowData) {

            let td = this.buildTd(objRowData[proparty]);
            tr.appendChild(td);

        }

        return tr;
    },

      buildTableBody: function (collection) {
        let tbody = document.createElement('TBODY');
        for (let data of collection) {

            let tr = this.buildTr(data);
            tbody.appendChild(tr);

        }

        return tbody;
    },

    buildTable : function (collection) {
       let table = document.createElement('table'); 
       let titles = Object.keys(collection[0]);// collecting table heads from data(here collection is data)
       let tHead = John.HtmlTable.buildTableHead(titles);// making table heads
       let tBody = John.HtmlTable.buildTableBody(collection);
       table.appendChild(tHead);
       table.appendChild(tBody);
       // table.border = 1; // trivial way
       table.setAttribute('border', '1'); // Nodes discussion
    
       return table;
    },

    display : function (data,domLocation) {
       let container = document.querySelector(domLocation); 
       let table = John.HtmlTable.buildTable(data);
       container.appendChild(table);
    }
};

John.HtmlTable.display(data,'#root');