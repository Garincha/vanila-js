"use strict";

const memberName = [{
    "PersonID": 5797,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/42332640174",
    "Notes": "",
    "BirthDate": "1962-11-28T00:00:00",
    "ParliamentaryName": "Ballantyne, Michelle",
    "PreferredName": "Michelle",
    "GenderTypeID": 1,
    
},/*
{
    "PersonID": 5800,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/44183378552",
    "Notes": "",
    "BirthDate": null,
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Halcro Johnston, Jamie",
    "PreferredName": "Jamie",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 5809,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/35783688782",
    "Notes": "",
    "BirthDate": null,
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Lockhart, Dean",
    "PreferredName": "Dean",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 5815,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/35781571595",
    "Notes": "",
    "BirthDate": null,
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Cole-Hamilton, Alex",
    "PreferredName": "Alex",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 5866,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/35781571135",
    "Notes": "",
    "BirthDate": "1962-11-29T00:00:00",
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Stewart, Alexander",
    "PreferredName": "Alexander",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 5868,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/35783688599",
    "Notes": "",
    "BirthDate": null,
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Arthur, Tom",
    "PreferredName": "Tom",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 5877,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/35783688260",
    "Notes": "",
    "BirthDate": "1967-03-11T00:00:00",
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Balfour, Jeremy",
    "PreferredName": "Jeremy",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 5929,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/42819976812",
    "Notes": "",
    "BirthDate": "1942-12-31T00:00:00",
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Mason, Tom",
    "PreferredName": "Tom",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 6090,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/39689732792",
    "Notes": "",
    "BirthDate": null,
    "BirthDateIsProtected": false,
    "ParliamentaryName": "Bowman, Bill",
    "PreferredName": "Bill",
    "GenderTypeID": 2,
    "IsCurrent": true
},
{
    "PersonID": 15631,
    "PhotoURL": "http://scottishparliament.thirdlight.com/file/56730435752",
    "Notes": "",
    "BirthDate": null,
    "BirthDateIsProtected": true,
    "ParliamentaryName": "Wishart, Beatrice",
    "PreferredName": "Beatrice",
    "GenderTypeID": 1,
    "IsCurrent": true
}*/
];


for(let index in memberName){
    const memberNameDetails = "<li>"+index+":"+memberName[index]+"</li>";// getting position(by "<li>"+index+"</li>") & value (laptop[index])
    const container = document.getElementById('json_data_list');
    container.innerHTML += memberNameDetails;
}