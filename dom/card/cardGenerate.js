"use strict";

const John = {};

John.htmlCard = {
    displayImage : function(userImg){
        let img = document.createElement('img');
        img.setAttribute('src',userImg);
        img.setAttribute('style','width: 200px; height: 200px');
        return img;
    },
    displayHeading : function (id) {
        let headText = document.createTextNode(id);
        let h3 = document.createElement('h3');
        h3.appendChild(headText);
        return h3;
    },
    prepareCard : function (user) {
        let card = document.createElement('div');
        let cardImg = '';
        let cardHead = '';
        
        cardImg = this.displayImage(user.avatar_url);
        card.appendChild(cardImg);
        cardHead = this.displayHeading(user.login);
        card.appendChild(cardHead);
        card.setAttribute('style','display : inline-block');
        return card;
    },
    display : function (data, domLocaation) {
        let container = document.querySelector(domLocaation);
        for(let datum of data){
            let card = this.prepareCard(datum);
            container.appendChild(card);
        }
        
        return true;
    }
};