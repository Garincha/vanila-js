"use strict";

function loadData() {
    const url = "https://api.github.com/users";
    fetch(url)
        .then((resp) => resp.json())
        .then(function (users) {
            //let users = eval(data);
            John.htmlCard.display(users,'#root');
        });
}

window.addEventListener('load',function () {
   document.getElementById('btnLoadData').addEventListener('click',loadData); 
});