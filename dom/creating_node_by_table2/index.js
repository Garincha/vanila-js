"use strict";

const data = (function () {
    const products = [{
            id: 1,
            date: "2015-09-01T00:00:00",
            title: "European North Sea Energy Alliance",
            sponsor: "Christina McKelvie MSP"
        },
        {
            id: 2,
            date: "2015-09-01T00:00:00",
            title: "AS it is",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 3,
            date: "2015-09-01T00:00:00",
            title: "Building Capacity, Investing in Stroke Research",
            sponsor: "Dennis Robertson MSP"
        },
        {
            id: 4,
            date: "2015-09-02T00:00:00",
            title: "Celebrating Scotland's Small and Rural Towns",
            sponsor: "Margaret McCulloch MSP"
        },
        {
            id: 5,
            date: "2015-09-02T00:00:00",
            title: "50 years of supporting carers",
            sponsor: "Graeme Dey MSP"
        }
    ];

    return products;
})();

function createTableHead(headerText) {
    let headContent = document.createTextNode(headerText);
    let head = document.createElement("TH");
    head.appendChild(headContent);

    return head;
}

let table = document.createElement("table");

let tr = document.createElement("tr");
tr.appendChild(createTableHead("ID"));
tr.appendChild(createTableHead("Date"));
tr.appendChild(createTableHead("Title"));
tr.appendChild(createTableHead("Sponsor"));
table.appendChild(tr);

function createTableCell(cellText) {
    let cellContent = document.createTextNode(cellText);
    let cell = document.createElement("TD");
    cell.appendChild(cellContent);

    return cell;
}

for (const product of data) {
    let tr2 = document.createElement("tr");
    tr2.appendChild(createTableCell(product.id));
    tr2.appendChild(createTableCell(product.date));
    tr2.appendChild(createTableCell(product.title));
    tr2.appendChild(createTableCell(product.sponsor));
    table.appendChild(tr2);
}

console.log(table);
let container = document.querySelector("#root");
container.appendChild(table);