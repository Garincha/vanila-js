"use strict";

// Fetch URL Function
/*

//this part of code is for json() method
function loadData() {
    const url = //"https://raw.githubusercontent.com/benoitvallon/100-best-books/master/books.json";
    "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
    //"https://api.github.com/users";
    fetch(url)
        //.then((resp) => resp.text())
        .then((resp) => resp.json())
        .then(function (data) {
            //let books_data  = eval(data);// eval() method is insecured, not recommended to use. 
            //let books_data = JSON.parse(data);// this line works same like above line.
            //let books_data = data;
            //console.log(Object.keys(books_data)); by this line of code, we can see the property/key of the object/collection(array)
            //console.log(books_data);
            //John.htmlTable.display(books_data, '#root');
            John.htmlTable.display(data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});
*/

// this part of code for eval() method. 
function loadData() {
    const url = //"https://raw.githubusercontent.com/benoitvallon/100-best-books/master/books.json";
    "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
    //"https://api.github.com/users";
    fetch(url)
        .then((resp) => resp.text())
        //.then((resp) => resp.json())
        .then(function (data) {
            let countries  = eval(data);// eval() method is insecured, not recommended to use. 
            //let books_data = JSON.parse(data);// this line works same like above line.
            //let books_data = data;
            //console.log(Object.keys(books_data)); by this line of code, we can see the property/key of the object/collection(array)
            //console.log(books_data);
            John.htmlTable.display(countries, '#root');
            //John.htmlTable.display(data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});

function myFunction() {
    let list = document.getElementById("root");
    list.removeChild(list.childNodes[0]);
  }
/*
// this part of code is for JSON.parse() method
function loadData() {
    const url = //"https://raw.githubusercontent.com/benoitvallon/100-best-books/master/books.json";
    "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
    //"https://api.github.com/users";
    fetch(url)
        .then((resp) => resp.text())
        //.then((resp) => resp.json())
        .then(function (data) {
            //let books_data  = eval(data);// eval() method is insecured, not recommended to use. 
            let books_data = JSON.parse(data);// this line works same like above line.
            //let books_data = data;
            //console.log(Object.keys(books_data)); by this line of code, we can see the property/key of the object/collection(array)
            //console.log(books_data);
            John.htmlTable.display(books_data, '#root');
            //John.htmlTable.display(data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});
*/


/*
function loadData() {
    const url = "https://raw.githubusercontent.com/benoitvallon/100-best-books/master/books.json";
    fetch(url)
        .then((resp) => resp.text())
        .then(function (data) {
            let users  = eval(data);
            
        });
}
loadData();
window.addEventListener(function () {
    document.getElementById('btnLoadData').addEventListener('click', John.htmlTable.display(data, '#root'));
});

*/
