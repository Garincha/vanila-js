"use strict";

function loadData() {
    const url = "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
    fetch(url)
        .then((resp) => resp.text())
        //.then((resp) => resp.json())
        .then(function (data) {
            let books_data  = eval(data);// eval() method is insecured, not recommended to use. 
            //let books_data = JSON.parse(data);// this line works same like above line.
            //let books_data = data;
            //console.log(Object.keys(books_data)); by this line of code, we can see the property/key of the object/collection(array)
            //console.log(books_data);
            John.htmlTable.display(books_data, '#root');
            //John.htmlTable.display(data, '#root');
        });
}

window.addEventListener('load',function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData);
});

function myFunction() {
    let list = document.getElementById("root");
    list.removeChild(list.childNodes[0]);
  }