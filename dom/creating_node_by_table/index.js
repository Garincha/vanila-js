"use strict";

const data = (function (){
    const products = [
        {
            "id" : 1,
            "date" : '2015-09-01T00:00:00',
            "title" : 'European North Sea Energy Alliance',
            "sponsor" : 'Christina McKelvie MSP', 
        },
        {
            "id" : 2,
            "date" : '2015-09-01T00:00:00',
            "title" : 'AS it is',
            "sponsor" : 'Margaret McCulloch MSP', 
        },
        {
            "id" : 3,
            "date" : '2015-09-01T00:00:00',
            "title" : 'Building Capacity, Investing in Stroke Research',
            "sponsor" : 'Dennis Robertson MSP', 
        },
        {
            "id" : 4,
            "date" : '2015-09-02T00:00:00',
            "title" : "Celebrating Scotland's Small and Rural Towns",
            "sponsor" : 'Margaret McCulloch MSP', 
        },
        {
            "id" : 5,
            "date" : '2015-09-02T00:00:00',
            "title" : '50 years of supporting carers',
            "sponsor" : 'Graeme Dey MSP', 
        }
    ];

    return products;
})();

let table = document.createElement('table');

for(const product of data){

    let itemIdText = document.createTextNode(product.id);
    let itemIdTd = document.createElement('td');
    itemIdTd.appendChild(itemIdText);  

    let itemDateText = document.createTextNode(product.date);
    let itemDateTd = document.createElement('td');
    itemDateTd.appendChild(itemDateText);

    let itemTitleText = document.createTextNode(product.title);
    let itemTitleTd = document.createElement('td');
    itemTitleTd.appendChild(itemTitleText);

    let itemSponsorText = document.createTextNode(product.sponsor);
    let itemSponsorTd = document.createElement('td');
    itemSponsorTd.appendChild(itemSponsorText);

    let tr = document.createElement('tr');
    tr.appendChild(itemIdTd);
    tr.appendChild(itemDateTd);
    tr.appendChild(itemTitleTd);
    tr.appendChild(itemSponsorTd);

    table.appendChild(tr);

}

console.log(table);
let container = document.querySelector("#root");
container.appendChild(table);